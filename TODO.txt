Как можно улучшить java ?

1. Properties в каждом объекте

------------------------------------------------------------------------------------------------------

2. К дополнению к private, package, protected, public сделать видимось для отдельных классов ->

public class Computer {

    @Scope(include = {ComputerFactory.class})
    private Computer() {
    }

}

то-есть мы можем расширять видимость. Или например сужать ->

public class Computer {

    @Scope(exclude = {SomeClass.class})
    public Computer() {
    }

}

------------------------------------------------------------------------------------------------------

3. Тернарный оператор как void ->

Например такое не скомпилируется:
{
    condition ? locker.lock() : locker.unlock()
}

Вместо этого надо писать так:
{
    if (condition) {
        locker.lock();
    } else {
        locker.unlock();
    }
}

------------------------------------------------------------------------------------------------------

@ From Rostislav B. :)