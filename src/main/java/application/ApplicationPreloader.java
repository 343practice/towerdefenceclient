package application;

import application.game.utils.Utils;
import application.game.utils.WindowSettings;
import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ApplicationPreloader extends Preloader {
    private Stage stage;

    private static final double WIDTH = 400;
    private static final double HEIGHT = 400;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setAlwaysOnTop(true);

        Pane imageWrapper = new Pane(new ImageView(new Image("/loading.gif")));
        imageWrapper.setStyle("-fx-background-color: transparent");
        imageWrapper.setOpacity(0.8);

        BorderPane root = new BorderPane();
        root.setStyle("-fx-background-color: transparent");

        Label towerDefenceText = new Label("Tower Defence");
        Utils utils = new Utils();

        towerDefenceText.setTextFill(Color.ORANGE);
        towerDefenceText.setFont(utils.GAME_FONT_LARGE);
        towerDefenceText.setEffect(new Glow(2));

        Text loadingText = new Text("Loading...");

        loadingText.setFill(Color.GREEN);
        loadingText.setFont(utils.GAME_FONT_MEDIUM);
        loadingText.setEffect(new Glow(2));

        BorderPane.setAlignment(towerDefenceText, Pos.CENTER);
        BorderPane.setAlignment(loadingText, Pos.CENTER);

        root.getChildren().add(imageWrapper);
        root.setTop(towerDefenceText);
        root.setBottom(loadingText);

        primaryStage.setOnCloseRequest(event -> Platform.exit());

        primaryStage.setX((WindowSettings.WIDTH - WIDTH) / 2);
        primaryStage.setY((WindowSettings.HEIGHT - HEIGHT) / 2);

        FillTransition fillTransition = new FillTransition();
        fillTransition.setDuration(Duration.seconds(2));
        fillTransition.setShape(loadingText);
        fillTransition.setFromValue(Color.RED);
        fillTransition.setToValue(Color.BLUE);
        fillTransition.setAutoReverse(true);
        fillTransition.setCycleCount(Animation.INDEFINITE);

        fillTransition.play();


        primaryStage.setScene(new Scene(root, WIDTH, HEIGHT, Color.TRANSPARENT));
        primaryStage.show();
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification stateChangeNotification) {
        if (stateChangeNotification.getType() == StateChangeNotification.Type.BEFORE_START) {
            stage.hide();
        }
    }


}
