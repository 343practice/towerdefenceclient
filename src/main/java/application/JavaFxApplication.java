package application;

import application.game.components.managers.TransitionManager;
import application.game.components.panes.Scene;
import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class JavaFxApplication extends Application {

    @Value("${fullscreen}")
    private Boolean fullscreen;

    @Autowired
    private Scene scene;

    @Autowired
    private TransitionManager transitionManager;

    private static boolean isRun;

    private static String[] args;

    private ConfigurableApplicationContext context;

    @Override
    public void init() throws Exception {
        context = new SpringApplicationBuilder(getClass()).web(false).run(args);
        context.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setFullScreen(fullscreen);
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.setScene(scene);

        isRun = true;

        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        transitionManager.stop();
        context.close();
        System.exit(0);
    }

    public static void main(String[] args) {
        JavaFxApplication.args = args;
        LauncherImpl.launchApplication(JavaFxApplication.class, ApplicationPreloader.class, args);
    }

    public static boolean stopped() {
        return !isRun;
    }
}
