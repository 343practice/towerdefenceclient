package application.game.shapes;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.PathElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Path extends javafx.scene.shape.Path {

    public enum ElementType {MOVE_TO, LINE_TO, CLOSE_PATH}

    private double distance = -1;

    {
        setFill(Color.GREEN);
        setOpacity(0.3);
        setStrokeWidth(2);
    }

    public static ElementType getElementType(PathElement element) {
        if (element instanceof MoveTo) return ElementType.MOVE_TO;
        if (element instanceof LineTo) return ElementType.LINE_TO;
        if (element instanceof ClosePath) return ElementType.CLOSE_PATH;

        throw new IllegalArgumentException("Unknown element type");
    }

    public double getDistance() {
        if (distance != -1) return distance;
        recalculateDistance();

        return distance;
    }

    public void recalculateDistance() {
        List<Point2D> points = getPoints(getElements());
        distance = 0;

        for (int i = 1; i < points.size(); i++) {
            distance += points.get(i - 1).distance(points.get(i));
        }
    }

    public static List<Point2D> getPoints(List<PathElement> elements) {
        List<Point2D> points = new ArrayList<>();

        for (PathElement element : elements) {
            switch (getElementType(element)) {
                case MOVE_TO: points.add(toPoint2D((MoveTo) element)); break;
                case LINE_TO: points.add(toPoint2D((LineTo) element)); break;
                case CLOSE_PATH: break;

                default:
                    throw new IllegalStateException("Unknown PathElement elementType");
            }
        }

        return points;
    }

    public static Path getPath(List<Point2D> points) {
        Path path = new Path();
        path.setFill(null);
        path.getElements().addAll(getElements(points));
        return path;
    }
    public static List<PathElement> getElements(Point2D... points) {
        return getElements(Arrays.asList(points));
    }
    public static List<PathElement> getElements(List<Point2D> points) {
        List<PathElement> elements = new ArrayList<>(points.size());

        elements.add(toMoveTo(points.get(0)));
        for (int i = 1; i < points.size(); i++) {
            elements.add(toLineTo(points.get(i)));
        }

        return elements;
    }

    public static Point2D toPoint2D(MoveTo moveTo) {
        return new Point2D(moveTo.getX(), moveTo.getY());
    }

    public static Point2D toPoint2D(LineTo lineTo) {
        return new Point2D(lineTo.getX(), lineTo.getY());
    }

    public static MoveTo toMoveTo(Point2D point) {
        return new MoveTo(point.getX(), point.getY());
    }

    public static LineTo toLineTo(Point2D point) {
        return new LineTo(point.getX(), point.getY());
    }

}