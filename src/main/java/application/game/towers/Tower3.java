package application.game.towers;

import application.game.weapons.Weapon;
import javafx.scene.image.Image;

public class Tower3 extends Tower {

    public Tower3() {
        super(Type.MIXED, weapon(), "/game/tower3.png", 1000);
    }

    private static Weapon weapon() {
        return new Weapon(
                new Image("/game/weapon3.png", 40, 40, true, false),
                50,
                300,
                30,
                2
        );
    }

}
