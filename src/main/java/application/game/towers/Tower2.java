package application.game.towers;

import application.game.weapons.Weapon;
import javafx.scene.image.Image;

public class Tower2 extends Tower {

    public Tower2() {
        super(Type.MIXED, weapon(), "/game/tower2.png", 500);
    }

    private static Weapon weapon() {
        return new Weapon(
                new Image("/game/weapon2.png", 40, 40, true, false),
                20,
                300,
                30,
                1
        );
    }

}
