package application.game.towers;

import application.game.weapons.Weapon;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public abstract class Tower {

    public static final double RADIUS = 40;

    public enum Type {
        AIR, LAND, MIXED
    }

    private Type type;
    private Weapon weapon;
    private double price;

    private ImageView imageView;
    private Circle area;
    private Circle coverage;
    private Tooltip descriptionTip;

    private boolean isStopped;

    public Tower(Type type, Weapon weapon, String imageUrl, double price) {
        this.type = type;
        this.weapon = weapon;
        this.price = price;

        area = new Circle(RADIUS);
        coverage = new Circle(weapon.getShootRadius());

        area.setOpacity(0);
        coverage.setOpacity(0);
        coverage.setFill(Color.ORANGE);

        area.setMouseTransparent(true);
        coverage.setMouseTransparent(true);

        final double size = RADIUS * 2;
        imageView = new ImageView(new Image(imageUrl, size, size, true, false));

        area.translateXProperty().bind(imageView.translateXProperty().add(RADIUS));
        area.translateYProperty().bind(imageView.translateYProperty().add(RADIUS));

        coverage.translateXProperty().bind(imageView.translateXProperty().add(RADIUS));
        coverage.translateYProperty().bind(imageView.translateYProperty().add(RADIUS));

        descriptionTip = new Tooltip(getDescription());

        imageView.setOnMouseEntered(event -> coverage.setOpacity(0.4));
        imageView.setOnMouseExited(event -> coverage.setOpacity(0));
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Circle getArea() {
        return area;
    }

    public Circle getCoverage() {
        return coverage;
    }

    public Tooltip getDescriptionTip() {
        return descriptionTip;
    }

    public boolean isStopped() {
        return isStopped;
    }

    public void start() {
        isStopped = false;
    }

    public void stop() {
        isStopped = true;
    }

    public String getDescription() {
        return "Type: " + type.name() +
                "\nShoot radius: " + weapon.getShootRadius() +
                "\nDamage: " + weapon.getDamage() +
                "\nFire rate: " + weapon.getFireRate() +
                "\nPrice: " + price;
    }

}
