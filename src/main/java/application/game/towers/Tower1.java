package application.game.towers;

import application.game.weapons.Weapon;
import javafx.scene.image.Image;

public class Tower1 extends Tower {

    public Tower1() {
        super(Type.MIXED, weapon(), "/game/tower1.png", 300);
    }

    private static Weapon weapon() {
        return new Weapon(
                new Image("/game/weapon1.png", 40, 40, true, false),
                50,
                200,
                10,
                2
        );
    }

}
