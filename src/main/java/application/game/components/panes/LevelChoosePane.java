package application.game.components.panes;

import application.game.components.GameField;
import application.game.components.Locker;
import application.game.components.animation.AnimatedCircles;
import application.game.components.animation.TransitionAnimation;
import application.game.components.managers.EventsManager;
import application.game.components.managers.ImagesManager;
import application.game.components.managers.LevelsManager;
import application.game.levels.Level;
import application.game.utils.AllowedZone;
import application.game.utils.Utils;
import application.game.utils.WindowSettings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LevelChoosePane extends Pane {

    @Autowired
    private LevelsManager levelsManager;

    @Autowired
    private EventsManager eventsManager;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private TransitionAnimation transitionAnimation;

    @Autowired
    private ImagesManager imagesManager;

    @Autowired
    private AnimatedCircles animatedCircles;

    private static final double ITEM_HEIGHT = 300;
    private static final double SPACING = 20;

    @PostConstruct
    private void init() {
        setPrefSize(WindowSettings.WIDTH, WindowSettings.HEIGHT);

        setBackground(new Background(new BackgroundFill(Color.rgb(35, 40, 30), null, null)));
        animatedCircles.start(this);

        Utils utils = new Utils();

        Label selectLevelLabel = new Label("Levels");
        selectLevelLabel.setFont(utils.GAME_FONT_LARGE);
        selectLevelLabel.setTextFill(Color.WHITE);
        selectLevelLabel.translateXProperty().bind(
                new SimpleDoubleProperty(WindowSettings.WIDTH)
                        .divide(2)
                        .subtract(selectLevelLabel.widthProperty()
                        .divide(2))
        );
        selectLevelLabel.translateYProperty().bind(new SimpleDoubleProperty(WindowSettings.HEIGHT).divide(12));

        List<Item> items = levelsManager.getLevels()
                .stream()
                .map(Item::new)
                .collect(Collectors.toList());

        HBox hBox = new HBox();

        hBox.needsLayoutProperty().addListener((observable, oldValue, newValue) -> {
            double width = hBox.getWidth();
            double height = hBox.getHeight();

            hBox.setLayoutY((WindowSettings.HEIGHT - height) / 2);

            if (width <= WindowSettings.WIDTH) {
                hBox.setLayoutX((WindowSettings.WIDTH - width) / 2);
            } else {
                hBox.setLayoutX(SPACING);
                eventsManager.makeDraggable(this, hBox,
                        new AllowedZone(new SimpleDoubleProperty(WindowSettings.WIDTH - width - SPACING * 2), null, null, null)
                );
            }

        });

        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(SPACING);
        hBox.getChildren().addAll(items);

        getChildren().addAll(selectLevelLabel, hBox);
    }

    private class Item extends BorderPane {
        public Item(Level level) {
            Image image = imagesManager.loadImage(level.getImageResourceName(), -1, ITEM_HEIGHT);
            setCenter(new ImageView(image));
            this.setOnMouseClicked(event -> {
                if (!event.isStillSincePress()) {
                    return;
                }
                animatedCircles.stop();
                transitionAnimation.hide(rootPane, LevelChoosePane.this);
                rootPane.startGame(level);
            });
        }

    }

}