package application.game.components.panes;

import application.game.components.GameField;
import application.game.components.managers.DragManager;
import application.game.components.managers.EventsManager;
import application.game.towers.Tower;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Scene extends javafx.scene.Scene {

    @Autowired
    private DragManager dragManager;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private GameField gameField;

    @Autowired
    private EventsManager eventsManager;

    @Autowired
    private PauseMenu pauseMenu;

    private KeyboardState gameKeyboardState = new GameKeyboardState();
    private KeyboardState keyboardState = gameKeyboardState;

    @Autowired
    public Scene(RootPane rootPane) {
        super(rootPane);
        getStylesheets().add("/style.css");
        setOnKeyPressed(event -> keyboardState.handle(event.getCode()));
    }

    public void changeState(StateType type) {
        switch (type) {
            case GAME: keyboardState = gameKeyboardState;
        }
    }

    public enum StateType {
        GAME
    }

    private interface KeyboardState {
        void handle(KeyCode keyCode);
    }

    private class GameKeyboardState implements KeyboardState {

        @Override
        public void handle(KeyCode keyCode) {
            if (keyCode == KeyCode.ESCAPE) {
                Tower tower = dragManager.towerProperty.get();
                if (tower != null) {
                    eventsManager.disableMovement(rootPane);
                    gameField.removeTower(tower);
                    dragManager.towerProperty.set(null);
                } else {
                    if (!rootPane.isGameStarted()) return;
                    if (!pauseMenu.isShown()) {
                        pauseMenu.show();
                    } else {
                        pauseMenu.hide();
                    }
                }
            }
        }
    }

}