package application.game.components.panes.game_field;

import application.game.components.GameField;
import application.game.utils.Utils;
import application.game.utils.WindowSettings;
import javafx.beans.property.*;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class TopBar extends Pane {

    @Autowired
    private GameField gameField;

    private static final double PADDING = 30;

    private Label levelLabel = new Label();
    private Label waveLabel = new Label();
    private Label healthLabel = new Label();
    private Label moneyLabel = new Label();

    @PostConstruct
    public void init() {
        setPrefSize(WindowSettings.WIDTH, 50);

        gameField.levelProperty.addListener((observable, oldValue, newValue) -> {
            if (oldValue != null) {
                levelLabel.textProperty().unbind();
                waveLabel.textProperty().unbind();
                healthLabel.textProperty().unbind();
                moneyLabel.textProperty().unbind();
            }

            levelLabel.textProperty().bind(new SimpleStringProperty("Level ").concat(newValue.indexProperty));
            waveLabel.textProperty().bind(new SimpleStringProperty("Wave: ").concat(newValue.waveProperty).concat("/" + gameField.getLevel().getMaxWaves()));
            healthLabel.textProperty().bind(new SimpleStringProperty("Health: ").concat(newValue.healthProperty));
            moneyLabel.textProperty().bind(new SimpleStringProperty("Money: ").concat(newValue.moneyProperty));
        });

        decorateLabels(levelLabel, waveLabel, healthLabel, moneyLabel);

        getChildren().addAll(levelLabel, waveLabel, healthLabel, moneyLabel);

        levelLabel.setTranslateX(PADDING);
        waveLabel.translateXProperty().bind(new SimpleDoubleProperty(WindowSettings.WIDTH)
                .divide(2)
                .subtract(waveLabel.widthProperty().multiply(2))
        );
        healthLabel.translateXProperty().bind(new SimpleDoubleProperty(WindowSettings.WIDTH)
                .divide(2)
        );
        moneyLabel.translateXProperty().bind(new SimpleDoubleProperty(WindowSettings.WIDTH - PADDING)
                .subtract(moneyLabel.widthProperty())
        );
    }

    private void decorateLabels(Label... labels) {
        Utils utils = new Utils();

        for (Label label : labels) {
            label.setFont(utils.GAME_FONT_MEDIUM);
            label.setTextFill(Color.WHITE);
        }
    }

}

