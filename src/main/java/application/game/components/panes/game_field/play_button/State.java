package application.game.components.panes.game_field.play_button;

import javafx.scene.image.Image;

public interface State {
    void click();
    Image getImage();
}
