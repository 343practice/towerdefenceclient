package application.game.components.panes.game_field.play_button;

import application.game.components.managers.enemies.WaveManager;
import javafx.scene.image.ImageView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PlayPauseButton extends ImageView {

    @Autowired
    private WaveManager waveManager;

    @Autowired
    @Qualifier("pause")
    private State pauseState;

    @Autowired
    @Qualifier("play")
    private State playState;

    private State currentState;

    public void reset() {
        setState(playState);

        setOnMouseClicked(event -> {
            waveManager.startWaves();
            changeState();

            setOnMouseClicked(e -> {
                currentState.click();
                changeState();
            });
        });
    }

    private void setState(State state) {
        currentState = state;
        setImage(state.getImage());
    }

    private void changeState() {
        if (currentState == playState) setState(pauseState);
        else if (currentState == pauseState) setState(playState);
    }

}
