package application.game.components.panes.game_field;

import application.game.components.managers.GameBarItemsManager;
import application.game.utils.WindowSettings;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class GameBar extends HBox {

    private static final double BORDER_SPACING_H = 50;
    private static final double BORDER_SPACING_V = 10;
    private static final double INSIDE_SPACING = 20;

    @Autowired
    private GameBarItemsManager itemsManager;

    @PostConstruct
    private void init() {

        parentProperty().addListener((observable, oldValue, newValue) -> {
            translateYProperty().bind(heightProperty().subtract(WindowSettings.HEIGHT).negate());
        });

        ImageView button1 = itemsManager.getButton(1);
        ImageView tower1 = itemsManager.getTower(1);
        ImageView tower2 = itemsManager.getTower(2);
        ImageView tower3 = itemsManager.getTower(3);

        HBox leftHBox = new HBox();
        HBox rightHBox = new HBox();

        leftHBox.setSpacing(INSIDE_SPACING);
        rightHBox.setSpacing(INSIDE_SPACING);

        leftHBox.getChildren().addAll(button1);
        rightHBox.getChildren().addAll(tower1, tower2, tower3);

        getChildren().addAll(leftHBox, rightHBox);

        leftHBox.translateXProperty().bind(new SimpleDoubleProperty(BORDER_SPACING_H));
        leftHBox.translateYProperty().bind(new SimpleDoubleProperty(-BORDER_SPACING_V));

        rightHBox.translateYProperty().bind(new SimpleDoubleProperty(-BORDER_SPACING_V));
        rightHBox.translateXProperty().bind(
                new SimpleDoubleProperty(WindowSettings.WIDTH)
                        .subtract(rightHBox.widthProperty())
                        .subtract(leftHBox.widthProperty())
                        .subtract(BORDER_SPACING_H)
        );

    }

}
