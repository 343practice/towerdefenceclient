package application.game.components.panes.game_field.play_button;

import application.game.components.managers.GameBarItemsManager;
import application.game.components.managers.ImagesManager;
import application.game.components.managers.TransitionManager;
import javafx.scene.image.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("pause")
public class PauseState implements State {

    private Image image;

    @Autowired
    private ImagesManager imagesManager;

    @Autowired
    private TransitionManager transitionManager;

    @PostConstruct
    private void init() {
        this.image = imagesManager.loadImage("/game/pause.png", GameBarItemsManager.SIZE);
    }

    @Override
    public void click() {
        transitionManager.pause();
    }

    @Override
    public Image getImage() {
        return image;
    }

}
