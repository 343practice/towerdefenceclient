package application.game.components.panes;

import application.game.components.ElementBuilder;
import application.game.components.animation.TransitionAnimation;
import application.game.components.managers.TransitionManager;
import application.game.utils.WindowSettings;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PauseMenu extends BorderPane {

    @Autowired
    private ElementBuilder elementBuilder;

    @Autowired
    private TransitionAnimation transitionAnimation;

    @Autowired
    private TransitionManager transitionManager;

    @Autowired
    private RootPane rootPane;

    private boolean isShown;

    @PostConstruct
    private void init() {
        setPrefSize(WindowSettings.WIDTH, WindowSettings.HEIGHT);
        setStyle("-fx-background-color: rgba(105, 105, 105, 0.5)");

        final double opacity = 0.5;
        final Color buttonColor = Color.DARKBLUE;
        final Color textColor = Color.WHITE;

        Region resume = elementBuilder.getButton("Resume", opacity, textColor, buttonColor);
        resume.setOnMouseClicked(event -> hide());

        Region changeLevel = elementBuilder.getButton("Change level", opacity, textColor, buttonColor);
        changeLevel.setOnMouseClicked(event -> rootPane.endGame());

        Region exit = elementBuilder.getButton("Exit", opacity, textColor, buttonColor);
        exit.setOnMouseClicked(event -> Platform.exit());

        VBox vBox = new VBox(resume, changeLevel, exit);
        vBox.setFillWidth(false);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(20);

        setCenter(vBox);
    }

    public void hide() {
        if (transitionAnimation.isLocked()) return;
        isShown = false;
        transitionAnimation.hide(rootPane, this, () -> transitionManager.play());
    }

    public void show() {
        if (transitionAnimation.isLocked()) return;
        transitionManager.pause();
        isShown = true;

        transitionAnimation.show(rootPane, this);
    }

    public boolean isShown() {
        return isShown;
    }

}
