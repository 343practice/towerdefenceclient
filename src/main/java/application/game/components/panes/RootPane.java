package application.game.components.panes;

import application.game.components.GameField;
import application.game.components.animation.AnimatedCircles;
import application.game.components.animation.TransitionAnimation;
import application.game.components.managers.EventsManager;
import application.game.components.managers.TransitionManager;
import application.game.components.managers.enemies.WaveManager;
import application.game.components.panes.game_field.GameBar;
import application.game.components.panes.game_field.play_button.PlayPauseButton;
import application.game.components.panes.game_field.TopBar;
import application.game.levels.Level;
import application.game.utils.WindowSettings;
import javafx.beans.value.ChangeListener;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.UUID;

@Component
public class RootPane extends Pane {

    @Autowired
    private GameField gameField;

    @Autowired
    private TopBar topBar;

    @Autowired
    private GameBar gameBar;

    @Autowired
    private EventsManager eventsManager;

    @Autowired
    private LevelChoosePane levelChoosePane;

    @Autowired
    private PauseMenu pauseMenu;

    @Autowired
    private PlayPauseButton playPauseButton;

    @Autowired
    private TransitionAnimation transitionAnimation;

    @Autowired
    private TransitionManager transitionManager;

    @Autowired
    private AnimatedCircles animatedCircles;

    @Autowired
    private WaveManager waveManager;

    private Scale contentScaleTransform = new Scale(1, 1);

    private UUID levelId;

    private ChangeListener<Number> finishGameListener = (observable, oldValue, newValue) -> {
        if (newValue.doubleValue() <= 0) {
            endGame();
        }
    };

    @PostConstruct
    private void init() throws IOException {
        setPrefSize(WindowSettings.WIDTH, WindowSettings.HEIGHT);
        gameField.levelProperty.addListener((observable, oldValue, newValue) -> resize());
        initContentPane(gameField);

        needsLayoutProperty().addListener(observable -> resize());

        getChildren().add(levelChoosePane);
    }

    public void startGame(Level level) {
        TransitionManager.setPause(false);
        levelId = UUID.randomUUID();
        playPauseButton.reset();

        Level prevLevel = gameField.getLevel();

        if (prevLevel != null) {
            prevLevel.healthProperty.removeListener(finishGameListener);
        }

        level.healthProperty.addListener(finishGameListener);

        gameField.setLevel(level);
        getChildren().addAll(topBar, gameBar);
        eventsManager.makeDraggable(this, gameField);
    }

    public void endGame() {
        levelId = null;
        eventsManager.disableDrag(this);
        transitionManager.stop();

        animatedCircles.start(levelChoosePane);
        transitionAnimation.start(this, pauseMenu, levelChoosePane, () -> {
            gameField.setLevel(null);
            getChildren().removeAll(topBar, gameBar);
        });
    }

    public boolean isGameStarted() {
        return levelId != null;
    }

    public UUID getLevelId() {
        return levelId;
    }

    public final void initContentPane(Pane contentPane) {
        contentPane.setManaged(false);

        contentPane.getTransforms().clear();
        contentPane.getTransforms().add(contentScaleTransform);
        getChildren().add(contentPane);
    }

    private Pane getContentPane() {
        return gameField;
    }

    public void resize() {
        double realWidth = getContentPane().prefWidth(getHeight());
        double realHeight = getContentPane().prefHeight(getWidth());

        double leftAndRight = getInsets().getLeft() + getInsets().getRight();
        double topAndBottom = getInsets().getTop() + getInsets().getBottom();

        double contentWidth = getWidth() - leftAndRight;
        double contentHeight = getHeight() - topAndBottom;

        double contentScaleWidth = contentWidth / realWidth;
        double contentScaleHeight = contentHeight / realHeight;

        double ratio = realWidth / realHeight;

        if (WindowSettings.RATIO > ratio) {
            gameField.propertyDragX.set(0);
            gameField.propertyDragY.set(WindowSettings.HEIGHT - realHeight * contentScaleWidth);

            setScale(contentScaleWidth);
        } else if (WindowSettings.RATIO < ratio) {
            gameField.propertyDragX.set(WindowSettings.WIDTH - realWidth * contentScaleHeight);
            gameField.propertyDragY.set(0);

            setScale(contentScaleHeight);
        } else {
            gameField.propertyDragX.set(0);
            gameField.propertyDragY.set(0);

            setScale(contentScaleWidth, contentScaleHeight);
        }

        getContentPane().relocate(getInsets().getLeft(), getInsets().getTop());
        getContentPane().resize(realWidth, realHeight);
    }

    private void setScale(double value) {
        setScale(value, value);
    }

    private void setScale(double x, double y) {
        contentScaleTransform.setX(x);
        contentScaleTransform.setY(y);
    }

    public void finishLevel() {
        endGame();
    }

    public double adaptX(double x) {
        return x / contentScaleTransform.getX();
    }

    public double adaptY(double y) {
        return y / contentScaleTransform.getY();
    }

}
