package application.game.components;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = "prototype")
public class Locker {

    private boolean lock;

    public boolean isLocked() {
        return lock;
    }

    public synchronized void setLock(boolean lock) {
        this.lock = lock;
    }

    public synchronized void lock() {
        this.lock = true;
    }

    public synchronized void unlock() {
        this.lock = false;
    }

}
