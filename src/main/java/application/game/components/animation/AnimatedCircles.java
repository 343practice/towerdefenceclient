package application.game.components.animation;

import application.game.components.Locker;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class AnimatedCircles {

    private static final int spawnNodes = 1000;
    private static final int circleSize = 10;

    @Autowired
    private Locker locker;

    static Color[] colors = {
            new Color(0.2, 0.5, 0.8, 1.0),
            new Color(0.3, 0.2, 0.7, 1.0),
            new Color(0.8, 0.3, 0.9, 1.0),
            new Color(0.4, 0.3, 0.9, 1.0),
            new Color(0.2, 0.5, 0.7, 1.0)
    };

    public void start(Pane container) {
        if (locker.isLocked()) return;
        locker.lock();
        for (int i = 0; i < spawnNodes; i++) {
            spawnNode(container);
        }
    }

    public void stop() {
        locker.unlock();
    }

    private void spawnNode(Pane container) {

        Circle node = new Circle();
        node.setManaged(false);
        node.setMouseTransparent(true);

        node.setFill(colors[(int) (Math.random() * colors.length)]);
        node.setCenterX(Math.random() * container.getPrefWidth());
        node.setCenterY(Math.random() * container.getPrefHeight());
        container.getChildren().add(node);
        node.toBack();

        Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.ZERO,
                        new KeyValue(node.radiusProperty(), 0),
                        new KeyValue(node.centerXProperty(), node.getCenterX()),
                        new KeyValue(node.centerYProperty(), node.getCenterY()),
                        new KeyValue(node.opacityProperty(), 0)),
                new KeyFrame(
                        Duration.seconds(5 + Math.random() * 5),
                        new KeyValue(node.opacityProperty(), Math.random()),
                        new KeyValue(node.radiusProperty(), Math.random() * circleSize)),
                new KeyFrame(
                        Duration.seconds(10 + Math.random() * 20),
                        new KeyValue(node.radiusProperty(), 0),
                        new KeyValue(node.centerXProperty(), Math.random() * container.getPrefWidth()),
                        new KeyValue(node.centerYProperty(), Math.random() * container.getPrefHeight()),
                        new KeyValue(node.opacityProperty(), 0))
        );

        timeline.setCycleCount(1);

        timeline.setOnFinished(event -> {
            container.getChildren().remove(node);
            if (locker.isLocked()) {
                spawnNode(container);
            }
        });

        timeline.play();

    }



}
