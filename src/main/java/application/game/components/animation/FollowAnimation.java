package application.game.components.animation;

import application.game.components.managers.TransitionManager;
import application.game.utils.Condition;
import application.game.utils.Utils;
import javafx.animation.AnimationTimer;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.util.Duration;

public class FollowAnimation extends AnimationTimer {

    private Node node;
    private Node target;
    private double speed;

    private Duration delay = Duration.ZERO;
    private Duration delayAfterFollowing = Duration.seconds(1);

    private boolean followAlways = false;

    private double acceleration = 0;
    private double passedPath = 0;

    private Runnable onEveryReach = () -> {};
    private Runnable onFinish = () -> {};
    private Runnable onHandle = () -> {};
    private Condition stopCondition = () -> true;

    private boolean rotateWithDirection = false;

    @Override
    public void handle(long now) {
        if (TransitionManager.isPause()) {
            return;
        }

        onHandle.run();

        Point2D nodePosition = getNodePosition();
        Point2D targetPosition = getTargetPosition();

        double distance = nodePosition.distance(targetPosition);

        if (distance == 0) {
            finish();
            return;
        }

        double factor = speed / distance;

        factor += acceleration * passedPath * factor;

        double targetX = targetPosition.getX();
        double targetY = targetPosition.getY();

        if (factor > 1) {
            node.setTranslateX(targetX);
            node.setTranslateY(targetY);

            finish();
            onEveryReach.run();
            passedPath = 0;
            return;
        }

        passedPath += factor;

        double nodeX = nodePosition.getX();
        double nodeY = nodePosition.getY();

        double deltaX = targetX - nodeX;
        double deltaY = targetY - nodeY;

        node.setTranslateX(nodeX + deltaX * factor);
        node.setTranslateY(nodeY + deltaY * factor);

        if (rotateWithDirection) {
            node.setRotate(Math.toDegrees(Utils.getAngle_Radians(nodePosition, targetPosition)) + 90);
        }

        if (!stopCondition.isForbidden()) {
            stop();
        }

    }

    private void finish() {
        if (followAlways) {
            Utils.delay(delayAfterFollowing, this::start, true);
        } else {
            onFinish.run();
        }

        stop();
    }

    private Point2D getNodePosition() {
        return new Point2D(node.getTranslateX(), node.getTranslateY());
    }

    private Point2D getRotatedNodePosition() {
        return new Point2D(node.getLocalToParentTransform().getTx(), node.getLocalToParentTransform().getTy());
    }

    private Point2D getTargetPosition() {

        double nodeWidth = node.getBoundsInParent().getWidth();
        double nodeHeight = node.getBoundsInParent().getHeight();

        double targetWidth = target.getBoundsInParent().getWidth();
        double targetHeight = target.getBoundsInParent().getHeight();

        double xOffset = (targetWidth - nodeWidth) / 2;
        double yOffset = (targetHeight - nodeHeight) / 2;

        return new Point2D(
                target.getTranslateX() + xOffset,
                target.getTranslateY() + yOffset
        );
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public void setTarget(Node target) {
        this.target = target;
    }

    public Node getNode() {
        return node;
    }

    public Node getTarget() {
        return target;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public void setDelay(Duration delay) {
        this.delay = delay;
    }

    public void enableFollowingAlways() {
        enableFollowingAlways(Duration.seconds(1));
    }

    public void enableFollowingAlways(Duration delayAfterFollowing) {
        this.followAlways = true;
        this.delayAfterFollowing = delayAfterFollowing;
    }

    public void enableFollowingAlways(Duration delayAfterFollowing, Runnable onEveryReach) {
        enableFollowingAlways(delayAfterFollowing);
        if (onEveryReach != null) {
            this.onEveryReach = onEveryReach;
        }
    }

    public void disableFollowingAlways() {
        this.followAlways = false;
    }

    public void setOnFinish(Runnable onFinish) {
        if (onFinish != null) {
            this.onFinish = onFinish;
        }
    }

    public void setOnHandle(Runnable onHandle) {
        if (onHandle != null) {
            this.onHandle = onHandle;
        }
    }

    public void setStopCondition(Condition stopCondition) {
        if (stopCondition != null) {
            this.stopCondition = stopCondition;
        }
    }

    public void setRotateWithDirection(boolean rotateWithDirection) {
        this.rotateWithDirection = rotateWithDirection;
    }

    public void play() {
        Utils.delay(delay, super::start, true);
    }

}
