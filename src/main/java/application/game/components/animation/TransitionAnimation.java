package application.game.components.animation;

import application.game.components.Locker;
import application.game.components.panes.PauseMenu;
import javafx.animation.FadeTransition;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.locks.ReentrantLock;

@Component
public class TransitionAnimation {

    private final Duration EFFECT_DURATION = Duration.millis(1000);

    private FadeTransition showAnimation = new FadeTransition(EFFECT_DURATION);
    private FadeTransition hideAnimation = new FadeTransition(EFFECT_DURATION);

    @Autowired
    private Locker pressLocker;

    @PostConstruct
    private void init() {
        showAnimation.setFromValue(0);
        showAnimation.setToValue(1);
        hideAnimation.setFromValue(1);
        hideAnimation.setToValue(0);
    }

    public void hide(Pane parent, Pane toHide) {
        hide(parent, toHide, null);
    }

    public void show(Pane parent, Pane toShow) {
        show(parent, toShow, null);
    }

    public void hide(Pane parent, Pane toHide, Runnable onFinish) {
        hideAnimation.setOnFinished(event -> {
            pressLocker.unlock();
            parent.getChildren().remove(toHide);

            if (onFinish != null) onFinish.run();
        });

        singleAnimation(hideAnimation, toHide);
    }

    public void show(Pane parent, Pane toShow, Runnable onFinish) {
        ObservableList<Node> children = parent.getChildren();
        if (!children.contains(toShow)) {
            children.add(toShow);
        }

        toShow.setOpacity(0);

        showAnimation.setOnFinished(event -> {
            pressLocker.unlock();
            if (onFinish != null) onFinish.run();
        });
        singleAnimation(showAnimation, toShow);
    }

    private void singleAnimation(FadeTransition fadeTransition, Pane target) {
        if (pressLocker.isLocked()) return;
        pressLocker.lock();

        fadeTransition.setNode(target);
        fadeTransition.play();
    }

    public void start(Pane parent, Pane currentPane, Pane newPane) {
        start(parent, currentPane, newPane, null);
    }

    public void start(Pane parent, Pane currentPane, Pane newPane, Runnable onFinish) {
        if (currentPane == newPane || pressLocker.isLocked()) return;
        pressLocker.lock();
        showAnimation.setOnFinished(event -> {
            pressLocker.unlock();
            if (onFinish != null) onFinish.run();
        });

        hideAnimation.setNode(currentPane);
        showAnimation.setNode(newPane);

        hideAnimation.setOnFinished(event -> {
            newPane.setOpacity(0);
            if (parent instanceof BorderPane) {
                ((BorderPane) parent).setCenter(newPane);
            } else {
                parent.getChildren().remove(currentPane);
                parent.getChildren().add(newPane);
            }
            showAnimation.play();
        });

        hideAnimation.play();
    }

    public void start(Pane currentPane, Pane newPane) {
        if (currentPane == newPane || pressLocker.isLocked()) return;
        pressLocker.lock();
        showAnimation.setOnFinished(event -> pressLocker.unlock());

        hideAnimation.setNode(currentPane);
        showAnimation.setNode(newPane);

        Parent parent = currentPane.getParent();

        hideAnimation.setOnFinished(event -> {
            newPane.setOpacity(0);
            if (parent instanceof BorderPane) {
                ((BorderPane) parent).setCenter(newPane);
            } else {
                parent.getChildrenUnmodifiable().remove(currentPane);
                parent.getChildrenUnmodifiable().add(newPane);
            }
            showAnimation.play();
        });

        hideAnimation.play();
    }

    public boolean isLocked() {
        return pressLocker.isLocked();
    }
}