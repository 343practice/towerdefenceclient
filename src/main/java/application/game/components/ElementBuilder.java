package application.game.components;

import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.springframework.stereotype.Component;

@Component
public class ElementBuilder {

    private static final double ITEM_WIDTH = 300;
    private static final double ITEM_HEIGHT = 30;

    private static final double TRANSITION_DURATION = 0.5;
    private static final double TEXT_SIZE = 16;
    private static final Color BUTTON_COLOR = Color.web("0x2A7A2A");
    private static final Color BUTTON_TEXT_COLOR = Color.web("0x73D123");

    public Label createLabel(String text) {
        return new Label(text);
    }

    public TextField createTextFiled(String text) {
        TextField textField = new TextField();
        decorateFiled(textField, text);
        return textField;
    }

    public PasswordField createPasswordFiled(String text) {
        PasswordField passwordField = new PasswordField();
        decorateFiled(passwordField, text);
        return passwordField;
    }

    private void decorateFiled(TextField field, String text) {
        field.setMinHeight(ITEM_HEIGHT);
        field.setFocusTraversable(false);
        field.setPromptText(text);
        field.getStyleClass().add("field");

        field.setPrefSize(300, 1);
    }

    public Region getDefaultButton(String text) {
        return getButton(text, TEXT_SIZE, ITEM_WIDTH, ITEM_HEIGHT, 1);
    }

    public Region getButton(String text, double buttonOpacity, Color textColor, Color buttonColor) {
        return getButton(text, TEXT_SIZE, ITEM_WIDTH, ITEM_HEIGHT, buttonOpacity, textColor, buttonColor);
    }

    public Region getButton(String text, double textSize, double width, double height, double opacity) {
        return getButton(text, textSize, width, height, opacity, BUTTON_TEXT_COLOR, BUTTON_COLOR);
    }

    public Region getButton(String text, double textSize, double width, double height, double opacity, Color textColor, Color buttonColor) {
        StackPane stackPane = new StackPane();

        Rectangle bg = new Rectangle(width, height, buttonColor);
        bg.setOpacity(opacity);

        Text textButton = new Text(text);
        textButton.setFill(textColor);
        textButton.setFont(Font.font("Arial", FontWeight.BOLD, textSize));

        stackPane.setAlignment(Pos.CENTER);
        stackPane.getChildren().addAll(bg, textButton);
        FillTransition st = new FillTransition(Duration.seconds(TRANSITION_DURATION), bg);
        stackPane.setOnMouseEntered(event -> {
            st.setFromValue(Color.DARKGRAY);
            st.setToValue(Color.DARKGOLDENROD);
            st.setCycleCount(Animation.INDEFINITE);
            st.setAutoReverse(true);
            st.play();
        });
        stackPane.setOnMouseExited(event -> {
            st.stop();
            bg.setFill(buttonColor);
        });

        return stackPane;
    }

}
