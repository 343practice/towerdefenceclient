package application.game.components;

import application.game.components.managers.enemies.Enemy;
import application.game.levels.Level;
import application.game.towers.Tower;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import org.springframework.stereotype.Component;
import application.game.shapes.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameField extends Pane {

    private List<Tower> towers = new ArrayList<>();
    private List<Enemy> enemies = new ArrayList<>();

    public final ObjectProperty<Level> levelProperty = new SimpleObjectProperty<>();
    public final SimpleDoubleProperty propertyDragX = new SimpleDoubleProperty(0);
    public final SimpleDoubleProperty propertyDragY = new SimpleDoubleProperty(0);

    private ImageView imageView = new ImageView();

    public void setLevel(Level level) {
        Level prevLevel = levelProperty.get();
        if (prevLevel != null) {
            prevLevel.waveProperty.set(0);
            destroyLevel();
        }

        if (level == null) {
            return;
        }

        if (!getChildren().contains(imageView)) {
            getChildren().add(imageView);
        }

        setTranslateX(0);
        setTranslateY(0);

        level.resetMoney();
        level.resetHealth();

        Image image = level.getImage();
        setPrefSize(image.getWidth(), image.getHeight());

        levelProperty.set(level);
        imageView.setImage(image);

        addZone(level.getForbiddenZone());
        addZone(level.getEnemiesPath());
    }

    private void destroyLevel() {
        getChildren().clear();

        towers.forEach(Tower::stop);
        towers.clear();
        enemies.clear();
    }

    public Level getLevel() {
        return levelProperty.get();
    }

    public void addTower(Tower tower) {
        towers.add(tower);
        getChildren().add(tower.getArea());
        getChildren().add(tower.getCoverage());
        getChildren().add(tower.getImageView());
    }

    public void removeTower(Tower tower) {
        towers.remove(tower);
        getChildren().remove(tower.getArea());
        getChildren().remove(tower.getCoverage());
        getChildren().remove(tower.getImageView());
    }

    public void addEnemy(Enemy enemy) {
        enemies.add(enemy);
        getChildren().add(enemy);
        getChildren().add(enemy.getArea());
        getChildren().add(enemy.getHealthLine());

        enemy.getHealthLine().refresh();
    }

    public void removeEnemy(Enemy enemy) {
        enemies.remove(enemy);
        getChildren().remove(enemy);
        getChildren().remove(enemy.getArea());
        getChildren().remove(enemy.getHealthLine());
        getLevel().subtractHealth(enemy.getCurrentHealth());
    }

    private void addZone(Path zone) {
        getChildren().add(zone);
    }

    private void removeZone(Path zone) {
        getChildren().remove(zone);
    }

    public List<Tower> getTowers() {
        return towers;
    }

    public List<Enemy> getEnemies() {
        return enemies
                .stream()
                .filter(enemy -> !enemy.isDied())
                .collect(Collectors.toList());
    }

}
