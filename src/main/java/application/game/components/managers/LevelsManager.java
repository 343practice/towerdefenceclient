package application.game.components.managers;

import application.game.components.managers.enemies.WaveManager;
import application.game.levels.Level;
import application.game.levels.ways.Way;
import javafx.scene.image.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LevelsManager {

    @Autowired
    private WayManager wayManager;

    @Autowired
    private WaveManager waveManager;

    private Map<Integer, Level> levels = new HashMap<>();

    @PostConstruct
    private void init() {
        final int levelsNumber = 15;

        for (int i = 1; i <= levelsNumber; i++) {
            levels.put(i, createLevel(i));
        }
    }

    public Level getLevel(int levelIndex) {
        return levels.get(levelIndex);
    }

    public List<Level> getLevels() {
        return levels.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    private Level createLevel(int levelIndex) {
        String resourceName = getImageUrl(levelIndex);

        try {
            Level level = new Level();

            URL resource = getClass().getResource(resourceName);
            Image originalImage = new Image(resource.openStream());

            level.setIndex(levelIndex);
            level.setImageResourceName(resourceName);
            level.setImage(originalImage);

            Way way = wayManager.getWay(levelIndex);
            level.setEnemiesPath(way.getEnemyPath());
            level.setForbiddenZone(way.getForbiddenZone());

            level.setWaves(waveManager.getWaves(levelIndex));
            level.setInitialMoney(getInitialMoney(levelIndex));
            level.setInitialHealth(getInitialHealth(levelIndex));

            return level;
        } catch (IOException e) {
            throw new IllegalStateException("Resource '" + resourceName + "' not exists");
        }
    }

    private String getImageUrl(int levelIndex) {
        String pattern = "/levels/level_" + levelIndex;

        switch (levelIndex) {
            case 5: return pattern + ".png";

            default: return pattern + ".jpg";
        }
    }

    private double getInitialMoney(int levelIndex) {
        switch (levelIndex) {
            case 1: return 300;
            default: return 500;
        }
    }

    private double getInitialHealth(int levelIndex) {
        switch (levelIndex) {
            case 1: return 200;
            default: return 1000;
        }
    }

}