package application.game.components.managers;

import application.game.components.GameField;
import application.game.components.panes.RootPane;
import application.game.components.panes.game_field.play_button.PlayPauseButton;
import application.game.towers.Tower;
import application.game.utils.WindowSettings;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class GameBarItemsManager {

    public static final double SIZE = WindowSettings.WIDTH / 12;
    private final double INSET = 15;

    private Map<Integer, ImageView> buttons = new HashMap<>();
    private Map<Integer, ImageView> towers = new HashMap<>();

    @Autowired
    private GameField gameField;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private DragManager dragManager;

    @Autowired
    private PlayPauseButton playPauseButton;

    @Autowired
    private TowerManager towerManager;

    @PostConstruct
    public void init() {
        buttons.put(1, playPauseButton);

        towers.put(1, prepareTower("/game/tower1.png", 1));
        towers.put(2, prepareTower("/game/tower2.png", 2));
        towers.put(3, prepareTower("/game/tower3.png", 3));
    }

    private ImageView prepareTower(String url, int towerIndex) {
        ImageView towerImage = new ImageView(new Image(url, SIZE, SIZE, true, true));

        Tooltip towerDescription = towerManager.getTowerDescription(towerIndex);
        towerImage.setOnMouseEntered(event -> towerDescription.show(
                towerImage,
                event.getScreenX() + INSET,
                event.getScreenY() + INSET
                )
        );
        towerImage.setOnMouseExited(event -> towerDescription.hide());

        towerImage.setOnMouseClicked(event -> {
            Tower tower = towerManager.getTower(towerIndex);
            if (tower.getPrice() <= gameField.getLevel().getMoney()) {
                dragManager.grab(tower, rootPane.adaptX(event.getSceneX()), rootPane.adaptY(event.getSceneY()));
            }
        });
        return towerImage;
    }

    public ImageView getButton(int index) {
        return buttons.get(index);
    }

    public ImageView getTower(int index) {
        return towers.get(index);
    }

}
