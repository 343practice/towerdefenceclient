package application.game.components.managers;

import javafx.scene.Node;
import javafx.scene.effect.*;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class EffectManager {

    public enum EffectType { DANGER }

    private Map<EffectType, Effect> effects = new HashMap<>();

    @PostConstruct
    private void init() {
        Light.Distant light = new Light.Distant();
        light.setColor(Color.RED);

        effects.put(EffectType.DANGER, new Lighting(light));
    }

    public Effect getEffect(EffectType effectType) {
        return effects.get(effectType);
    }

    public void setEffect(Node node, EffectType effectType, boolean condition) {
        if (condition) {
            node.setEffect(getEffect(effectType));
        } else {
            node.setEffect(null);
        }
    }

}
