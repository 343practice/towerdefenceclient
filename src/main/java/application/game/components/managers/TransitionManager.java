package application.game.components.managers;

import application.game.components.managers.enemies.Enemy;
import application.game.utils.Utils;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.util.Duration;
import org.springframework.stereotype.Component;
import application.game.shapes.Path;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TransitionManager {

    private static final double SPEED_COEFFICIENT = 10;

    private Map<Node, List<Transition>> nodeTransitions = new HashMap<>();
    private static boolean isPause;

    public static boolean isPause() {
        return isPause;
    }

    public void add(Node node, Transition transition) {
        EventHandler<ActionEvent> onFinished = transition.getOnFinished();

        if (onFinished != null) {
            transition.setOnFinished(event -> {
                onFinished.handle(event);
                remove(node, transition);
            });
        } else {
            transition.setOnFinished(event -> remove(node, transition));
        }

        if (nodeTransitions.containsKey(node)) {
            List<Transition> transitions = nodeTransitions.get(node);
            transitions.add(transition);
        } else {
            nodeTransitions.put(node, Utils.toList(transition));
        }

    }

    public void remove(Node node, Transition transition) {
        nodeTransitions.get(node).remove(transition);
    }

    public void pause() {
        if (isPause) return;

        nodeTransitions.forEach((node, transitions) -> transitions.forEach(Animation::pause));
        isPause = true;
    }

    public void play() {
        if (!isPause) return;

        nodeTransitions.forEach((node, transitions) -> transitions.forEach(Animation::play));
        isPause = false;
    }

    public void stop() {
        isPause = true;
        nodeTransitions.forEach((node, transitions) -> transitions.forEach(Animation::stop));
    }

    public void stop(Node node) {
        List<Transition> transitions = nodeTransitions.get(node);
        if (transitions != null) {
            transitions.forEach(Animation::stop);
        }
    }

    public void playPathTransition(double speed, Path path, Enemy enemy, EventHandler<ActionEvent> onFinish) {
        Duration duration = calculateDuration(path.getDistance(), speed);

        PathTransition pathTransition = new PathTransition(duration, path, enemy);
        pathTransition.setCycleCount(1);
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setInterpolator(Interpolator.LINEAR);
        pathTransition.setOnFinished(onFinish);

        add(enemy, pathTransition);

        if (!isPause) {
            pathTransition.play();
            enemy.playGif();
        }
    }

    public Duration calculateDuration(double distance, double speed) {
        return calculateDuration(distance, speed, SPEED_COEFFICIENT);
    }

    public Duration calculateDuration(double distance, double speed, double speedCoefficient) {
        return Duration.seconds(distance / (speed * speedCoefficient));
    }

    public void pauseWrap(Duration duration, Runnable runnable) {
        pauseWrap(duration, runnable, false);
    }

    public void pauseWrap(Duration duration, Runnable runnable, boolean inFXThread) {
        Utils.pause(TransitionManager::isPause);

        if (inFXThread) {
            Platform.runLater(runnable);
        } else {
            runnable.run();
        }

        Utils.pause(duration, TransitionManager::isPause);
    }

    public static void setPause(boolean isPause) {
        TransitionManager.isPause = isPause;
    }
}