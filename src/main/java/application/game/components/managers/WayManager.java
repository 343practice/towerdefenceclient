package application.game.components.managers;

import application.game.levels.ways.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class WayManager {

    private Map<Integer, Way> ways = new HashMap<>();

    @PostConstruct
    private void initZones() {
        ways.put(1, new Way1());
        ways.put(2, new Way2());
        ways.put(3, new Way3());
        ways.put(4, new Way4());
        ways.put(5, new Way5());
        ways.put(6, new Way6());
        ways.put(7, new Way7());
        ways.put(8, new Way8());
        ways.put(9, new Way9());
        ways.put(10, new Way10());
        ways.put(11, new Way11());
        ways.put(12, new Way12());
        ways.put(13, new Way13());
        ways.put(14, new Way14());
        ways.put(15, new Way15());
    }

    public Way getWay(int index) {
        return ways.get(index);
    }

}