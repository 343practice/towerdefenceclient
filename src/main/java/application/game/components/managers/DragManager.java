package application.game.components.managers;

import application.game.components.GameField;
import application.game.components.Locker;
import application.game.components.animation.Offset;
import application.game.components.panes.RootPane;
import application.game.towers.Tower;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static application.game.components.managers.EffectManager.EffectType.DANGER;

@Component
public class DragManager {

    @Autowired
    private GameField gameField;

    @Autowired
    private EventsManager eventsManager;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private ImagesManager imagesManager;

    @Autowired
    private EffectManager effectManager;

    @Autowired
    private CollisionManager collisionManager;

    @Autowired
    private TowerManager towerManager;

    @Autowired
    private Locker locker;

    public ObjectProperty<Tower> towerProperty = new SimpleObjectProperty<>();

    public void grab(Tower tower, double cursorX, double cursorY) {
        ImageView towerImage = tower.getImageView();

        towerImage.setTranslateX(cursorX - Tower.RADIUS);
        towerImage.setTranslateY(cursorY - Tower.RADIUS);

        Offset dragCursorOffset = new Offset(Tower.RADIUS, Tower.RADIUS);

        eventsManager.disableDrag(rootPane);
        towerProperty.set(tower);
        gameField.addTower(tower);

        eventsManager.makeMovableByCursor(gameField, towerImage, dragCursorOffset, () -> {
            boolean condition = collisionManager.intersects(tower);
            locker.setLock(condition);
            effectManager.setEffect(towerImage, DANGER, condition);
        });

        gameField.setOnMouseClicked(event -> {
            if (locker.isLocked()) return;

            eventsManager.disableMovement(gameField);
            towerProperty.set(null);

            gameField.getLevel().subtractMoney(tower.getPrice());
            towerManager.start(tower);

            eventsManager.makeDraggable(rootPane, gameField);

            gameField.setOnMouseClicked(null);
        });

    }
}
