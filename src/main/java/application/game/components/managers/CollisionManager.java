package application.game.components.managers;

import application.game.components.GameField;
import application.game.towers.Tower;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CollisionManager {

    @Autowired
    private GameField gameField;

    public boolean intersects(Tower tower) {
        List<Tower> towers = gameField.getTowers()
                .stream()
                .filter(t -> !t.equals(tower))
                .collect(Collectors.toList());

        for (Tower t : towers) {
            if (intersects(t.getArea(), tower.getArea())) {
                return true;
            }
        }

        return intersectsWay(tower.getArea());
    }

    private boolean intersectsWay(Shape shape) {
        return intersects(shape, gameField.getLevel().getForbiddenZone());
    }

    public boolean intersects(Shape shape1, Shape shape2) {
        return ((Path) Shape.intersect(shape1, shape2)).getElements().size() > 0;
    }

}
