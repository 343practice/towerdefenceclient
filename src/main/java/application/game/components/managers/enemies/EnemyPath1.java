package application.game.components.managers.enemies;

import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class EnemyPath1 extends Path {

    private double distance;

    {
        getElements().add(new MoveTo(0, 1500));
        getElements().add(new LineTo(900, 1500));
        getElements().add(new LineTo(930, 530));
        getElements().add(new LineTo(1200, 350));
        getElements().add(new LineTo(2000, 350));
        getElements().add(new LineTo(2100, 450));
        getElements().add(new LineTo(2250, 900));
        getElements().add(new LineTo(2900, 900));

        distance = super.getDistance();
    }

    @Override
    public double getDistance() {
        return distance;
    }

}
