package application.game.components.managers.enemies.waves;

import application.game.components.managers.enemies.EnemyType;
import javafx.util.Duration;

public class SubWave {

    private EnemyType enemyType;
    private Duration timeToNext;
    private Duration creatingInterval;
    private int level;
    private int number;

    public SubWave(EnemyType enemyType, Duration timeToNext, Duration creatingInterval, int level, int number) {
        this.enemyType = enemyType;
        this.timeToNext = timeToNext;
        this.creatingInterval = creatingInterval;
        this.level = level;
        this.number = number;
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }

    public Duration getTimeToNext() {
        return timeToNext;
    }

    public Duration getCreatingInterval() {
        return creatingInterval;
    }

    public int getLevel() {
        return level;
    }

    public int getNumber() {
        return number;
    }

}
