package application.game.components.managers.enemies.waves.impls;

import application.game.components.managers.enemies.EnemyType;
import application.game.components.managers.enemies.waves.Wave;
import application.game.components.managers.enemies.waves.Waves;
import javafx.util.Duration;

import java.util.Arrays;

public class Waves2 extends Waves {

    {
        setInterval(Duration.seconds(5));

        Wave wave1 = new Wave();
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 2, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 2, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 3, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 3, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 4, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 4, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 5, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 5, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.7), 7, 7);

        Wave wave2 = new Wave();
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 9, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 9, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 10, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 9, 9);

        Wave wave3 = new Wave();
        wave3.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 11, 10);
        wave3.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 11, 10);
        wave3.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 12, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 12, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 13, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 13, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 14, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 15, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.4), 13, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.4), 14, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.4), 15, 9);

        setWaves(Arrays.asList(wave1, wave2, wave3));
    }

}
