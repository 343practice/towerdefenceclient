package application.game.components.managers.enemies;

import application.JavaFxApplication;
import application.game.components.GameField;
import application.game.components.Locker;
import application.game.components.managers.TransitionManager;
import application.game.components.managers.enemies.waves.*;
import application.game.components.managers.enemies.waves.impls.*;
import application.game.components.panes.RootPane;
import application.game.utils.Utils;
import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class WaveManager {

    private Map<Integer, Waves> waves = new HashMap<>();

    @Autowired
    private EnemiesManager enemiesManager;

    @Autowired
    private GameField gameField;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private TransitionManager transitionManager;

    private boolean isWavesFinished;

    @PostConstruct
    private void init() {
        waves.put(1, new Waves1());
        waves.put(2, new Waves2());
        waves.put(3, new Waves3());
        waves.put(4, new Waves4());
        waves.put(5, new Waves5());
        waves.put(6, new Waves6());
        waves.put(7, new Waves7());
        waves.put(8, new Waves8());
        waves.put(9, new Waves9());
        waves.put(10, new Waves10());
        waves.put(11, new Waves11());
        waves.put(12, new Waves12());
        waves.put(13, new Waves13());
        waves.put(14, new Waves14());
        waves.put(15, new Waves15());
    }

    public Waves getWaves(int levelIndex) {
        return waves.get(levelIndex);
    }

    private void startWave(Wave wave, UUID levelId) {
        List<SubWave> subSubWaves = wave.getSubSubWaves();

        for (SubWave subWave : subSubWaves) {
            if (isDestroy(levelId)) {
                isWavesFinished = true;
                return;
            }

            transitionManager.pauseWrap(subWave.getTimeToNext(), () -> startSubWave(subWave, levelId));
        }
    }

    private void startSubWave(SubWave subWave, UUID levelId) {
        for (int i = 0; i < subWave.getNumber(); i++) {
            if (isDestroy(levelId)) {
                isWavesFinished = true;
                return;
            }

            Runnable runnable = () -> {
                if (!isDestroy(levelId)) {
                    enemiesManager.createEnemy(subWave.getEnemyType(), subWave.getLevel());
                }
            };

            transitionManager.pauseWrap(subWave.getCreatingInterval(), runnable, true);
        }
    }

    public void startWaves() {
        startWaves(gameField.getLevel().getWaves());
    }

    public void startWaves(Waves waves) {
        isWavesFinished = false;
        Utils.inNewThread(() -> {
            UUID levelId = rootPane.getLevelId();

            for (Wave wave : waves.getWaves()) {
                if (isDestroy(levelId)) {
                    isWavesFinished = true;
                    return;
                }

                Platform.runLater(() -> gameField.getLevel().nextWave());
                transitionManager.pauseWrap(waves.getInterval(), () -> startWave(wave, levelId));
            }

            isWavesFinished = true;
        });

    }

    public boolean isWavesFinished() {
        return isWavesFinished;
    }

    private boolean isDestroy(UUID levelId) {
        return !levelId.equals(rootPane.getLevelId()) || JavaFxApplication.stopped();
    }

}
