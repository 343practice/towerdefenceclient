package application.game.components.managers.enemies.waves.impls;

import application.game.components.managers.enemies.EnemyType;
import application.game.components.managers.enemies.waves.Wave;
import application.game.components.managers.enemies.waves.Waves;
import javafx.util.Duration;

import java.util.Arrays;

public class Waves1 extends Waves {

    {
        setInterval(Duration.seconds(10));

        Wave wave1 = new Wave();
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.8), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 2, 5);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 3, 5);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 4, 5);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 4, 3);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 5, 2);
//        wave1.addSubWave(EnemyType.ENEMY_3, Duration.ZERO, Duration.seconds(0.7), 3, 5);

        Wave wave2 = new Wave();
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 4, 5);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 5, 5);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 3, 5);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 4, 4);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 5, 4);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 6, 3);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 7, 2);

        Wave wave3 = new Wave();
        wave3.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 7, 10);
        wave3.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.6), 8, 7);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 5, 7);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(3), Duration.seconds(0.6), 6, 7);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(3), Duration.seconds(0.6), 5, 5);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(3), Duration.seconds(0.6), 6, 5);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(3), Duration.seconds(0.6), 7, 5);

        setWaves(Arrays.asList(wave1, wave2, wave3));
    }

}
