package application.game.components.managers.enemies;

import application.game.components.GameField;
import application.game.components.managers.CollisionManager;
import application.game.components.managers.TransitionManager;
import application.game.components.panes.RootPane;
import application.game.towers.Tower;
import application.game.weapons.Weapon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import application.game.shapes.Path;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EnemiesManager {

    @Autowired
    private GameField gameField;

    @Autowired
    private RootPane rootPane;

    @Autowired
    private CollisionManager collisionManager;

    @Autowired
    private TransitionManager transitionManager;

    @Autowired
    private WaveManager waveManager;

    public void shot(double damage, Enemy enemy) {
        if (enemy.isDied()) return;

        enemy.healthSubtract(damage);

        if (enemy.isDied()) {
            gameField.removeEnemy(enemy);
            transitionManager.stop(enemy);
            enemy.stopGif();

            gameField.getLevel().addMoney(enemy.getPriceForKill());

            if (waveManager.isWavesFinished() && gameField.getEnemies().isEmpty()) {
                rootPane.finishLevel();
            }

            return;
        }

        enemy.getHealthLine().refresh();
    }

    public void shot(Weapon weapon, Enemy enemy) {
        shot(weapon.getDamage(), enemy);
    }

    public Enemy createEnemy(EnemyType enemyType) {
        return createEnemy(enemyType, 0);
    }

    public Enemy createEnemy(EnemyType enemyType, int level) {
        Enemy enemy = getEnemy(enemyType, level);
        gameField.addEnemy(enemy);

        Path enemyPath = gameField.getLevel().getEnemiesPath();

        transitionManager.playPathTransition(enemy.getSpeed(), enemyPath, enemy, onFinish -> {
            gameField.removeEnemy(enemy);
            transitionManager.stop(enemy);
            enemy.stopGif();

            if (waveManager.isWavesFinished() && gameField.getEnemies().isEmpty()) {
                rootPane.finishLevel();
            }
        });

        return enemy;
    }

    public Enemy getEnemy(EnemyType enemyType, int level) {
        switch (enemyType) {
            case ENEMY_1: return new Enemy1(level);
            case ENEMY_2: return new Enemy2(level);
            case ENEMY_3: return new Enemy3(level);
            case ENEMY_4: return new Enemy4(level);

            default: throw new IllegalArgumentException("Unknown enemy type");
        }
    }

    public List<Enemy> getEnemiesInTowerRange(Tower tower) {
        return gameField.getEnemies()
                .stream()
                .filter(enemy -> weaponTypeSuitable(enemy, tower) && isEnemyInRange(enemy, tower))
                .collect(Collectors.toList());
    }

    private boolean isEnemyInRange(Enemy enemy, Tower tower) {
        return collisionManager.intersects(enemy.getArea(), tower.getCoverage());
    }

    private boolean weaponTypeSuitable(Enemy enemy, Tower tower) {
        switch (tower.getType()) {
            case MIXED: return true;
            case LAND: return enemy.getMovingType() == MovingType.LAND;
            case AIR: return enemy.getMovingType() == MovingType.AIR;

            default: throw new IllegalStateException("Unknown tower type");
        }
    }

}