package application.game.components.managers.enemies;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

public class Enemy1 extends Enemy {

    private static final double DEFAULT_SIZE = 40;
    private static final double DEFAULT_MAX_HEALTH = 100;
    private static final double DEFAULT_SPEED = 15;
    private static final MovingType MOVING_TYPE = MovingType.LAND;
    private static final double PRICE_FOR_KILL = 10;
    private static final List<Image> images = new ArrayList<>();

    static {
        images.add(new Image("/enemies/enemy1/enemy1_1.png"));
        images.add(new Image("/enemies/enemy1/enemy1_2.png"));
    }

    public Enemy1() {
        super(images, DEFAULT_SIZE, DEFAULT_MAX_HEALTH, DEFAULT_SPEED, MOVING_TYPE, PRICE_FOR_KILL);
        setSleepTimeInMillis(300);
    }

    public Enemy1(int level) {
        this();
        upgrade(level);
    }

    @Override
    public void upgrade(int level) {
        if (level <= 0) return;

        changeMaxHealth(DEFAULT_MAX_HEALTH * level / 5);
        changePriceForKill( PRICE_FOR_KILL * level / ( ( PRICE_FOR_KILL / 10 ) * level )  * level );
    }

}
