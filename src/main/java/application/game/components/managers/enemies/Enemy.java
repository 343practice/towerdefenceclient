package application.game.components.managers.enemies;

import application.game.gif_images.GifImage;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

import java.util.List;

public abstract class Enemy extends GifImage {

    protected double maxHealth;

    protected double speed;
    protected double currentHealth;
    private MovingType movingType;
    private double priceForKill;
    private double size;
    private Rectangle area;

    private final HealthLine healthLine;

    public Enemy(List<Image> images, double size, double maxHealth, double speed, MovingType movingType, double priceForKill) {
        setFrames(images);

        this.maxHealth = maxHealth;
        this.speed = speed;
        this.movingType = movingType;
        this.priceForKill = priceForKill;

        this.currentHealth = maxHealth;
        this.healthLine = new HealthLine(this);

        this.size = size;
        area = new Rectangle(size, size);
        area.setOpacity(0);
        area.setMouseTransparent(true);

        area.translateXProperty().bind(translateXProperty());
        area.translateYProperty().bind(translateYProperty());
    }

    public double getMaxHealth() {
        return maxHealth;
    }

    public double getSpeed() {
        return speed;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public MovingType getMovingType() {
        return movingType;
    }

    public HealthLine getHealthLine() {
        return healthLine;
    }

    public double getPriceForKill() {
        return priceForKill;
    }

    public Rectangle getArea() {
        return area;
    }

    public double getWidth() {
        return this.size;
    }

    public double getHeight() {
        return this.size;
    }

    public void healthSubtract(double damage) {
        this.currentHealth -= damage;
    }

    public boolean isDied() {
        return this.currentHealth <= 0;
    }

    protected void changeMaxHealth(double maxHealth) {
        this.maxHealth = maxHealth;
        this.currentHealth = maxHealth;
    }

    protected void changePriceForKill(double priceForKill){
        this.priceForKill += priceForKill;
    }

    protected void changeSpeed(double speed) {
        this.speed = speed;
    }

    public abstract void upgrade(int level);

}