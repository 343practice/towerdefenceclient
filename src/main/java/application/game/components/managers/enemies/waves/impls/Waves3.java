package application.game.components.managers.enemies.waves.impls;

import application.game.components.managers.enemies.EnemyType;
import application.game.components.managers.enemies.waves.Wave;
import application.game.components.managers.enemies.waves.Waves;
import javafx.util.Duration;

import java.util.Arrays;

public class Waves3 extends Waves {

    {
        setInterval(Duration.seconds(5));

        Wave wave1 = new Wave();
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 2, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 2, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.4), 3, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 3, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 4, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 4, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 5, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 5, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 7, 10);

        Wave wave2 = new Wave();
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 5, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 5, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 6, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 6, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(1), Duration.seconds(0.3), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.4), 8, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 7, 10);
        wave2.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 9, 10);

        Wave wave3 = new Wave();
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.2), 10, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.2), 10, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.2), 11, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.2), 11, 10);
        wave3.addSubWave(EnemyType.ENEMY_2, Duration.seconds(1), Duration.seconds(0.2), 12, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 12, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 13, 10);
        wave3.addSubWave(EnemyType.ENEMY_3, Duration.seconds(1), Duration.seconds(0.4), 13, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.5), 14, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.5), 14, 10);
        wave3.addSubWave(EnemyType.ENEMY_4, Duration.seconds(1), Duration.seconds(0.5), 15, 10);

        setWaves(Arrays.asList(wave1, wave2));
    }

}
