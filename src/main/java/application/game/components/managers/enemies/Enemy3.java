package application.game.components.managers.enemies;

import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.List;

public class Enemy3 extends Enemy {

    private static final double DEFAULT_SIZE = 40;
    private static final double DEFAULT_MAX_HEALTH = 250;
    private static final double DEFAULT_SPEED = 15;
    private static final MovingType MOVING_TYPE = MovingType.LAND;
    private static final double PRICE_FOR_KILL = 40;
    private static final List<Image> images = new ArrayList<>();

    static {
        images.add(new Image("/enemies/enemy3/enemy3_4.png"));
    }

    public Enemy3() {
        super(images, DEFAULT_SIZE, DEFAULT_MAX_HEALTH, DEFAULT_SPEED, MOVING_TYPE, PRICE_FOR_KILL);
        setSleepTimeInMillis(800);
    }

    public Enemy3(int level) {
        this();
        upgrade(level);
    }

    @Override
    public void upgrade(int level) {
        if (level <= 0) return;

        changeMaxHealth(DEFAULT_MAX_HEALTH * level / 5);
        changePriceForKill( PRICE_FOR_KILL * level / ( ( PRICE_FOR_KILL / 10 ) * level )  * level );
    }

}
