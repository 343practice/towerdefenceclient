package application.game.components.managers.enemies;

import javafx.scene.layout.Pane;

public class HealthLine extends Pane {

    private static final double lineHeight = 5;
    private static final double spacing = 10;

    private final Enemy enemy;

    public HealthLine(Enemy enemy) {
        this.enemy = enemy;

        setPrefHeight(lineHeight);
        getStyleClass().add("health-line");

        translateXProperty().bind(enemy.translateXProperty());
        translateYProperty().bind(enemy.translateYProperty().subtract(spacing));
    }

    public void refresh() {
        double maxHealth = enemy.getMaxHealth();
        double currentHealth = enemy.getCurrentHealth();

        setMinWidth(enemy.getWidth() * currentHealth / maxHealth);
    }

}
