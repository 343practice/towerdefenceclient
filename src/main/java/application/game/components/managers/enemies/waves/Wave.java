package application.game.components.managers.enemies.waves;

import application.game.components.managers.enemies.EnemyType;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class Wave {

    private List<SubWave> subSubWaves = new ArrayList<>();

    public void addSubWave(EnemyType enemyType, Duration timeToNext, Duration creatingInterval, int enemyLevel, int number) {
        subSubWaves.add(new SubWave(enemyType, timeToNext, creatingInterval, enemyLevel, number));
    }

    protected void addSubWave(SubWave subWave, int number) {
        subSubWaves.add(subWave);
    }

    public List<SubWave> getSubSubWaves() {
        return subSubWaves;
    }

}