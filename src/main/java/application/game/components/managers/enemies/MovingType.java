package application.game.components.managers.enemies;

public enum MovingType {
    LAND, AIR
}
