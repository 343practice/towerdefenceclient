package application.game.components.managers.enemies.waves;

import javafx.util.Duration;

import java.util.List;

public abstract class Waves {

    private List<Wave> waves;
    private Duration interval;

    public Waves() {
    }

    public Waves(List<Wave> waves, Duration interval) {
        this.waves = waves;
        this.interval = interval;
    }

    public List<Wave> getWaves() {
        return waves;
    }

    public void setWaves(List<Wave> waves) {
        this.waves = waves;
    }

    public Duration getInterval() {
        return interval;
    }

    public void setInterval(Duration interval) {
        this.interval = interval;
    }

}