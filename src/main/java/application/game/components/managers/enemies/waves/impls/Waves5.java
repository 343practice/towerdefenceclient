package application.game.components.managers.enemies.waves.impls;

import application.game.components.managers.enemies.EnemyType;
import application.game.components.managers.enemies.waves.Wave;
import application.game.components.managers.enemies.waves.Waves;
import javafx.util.Duration;

import java.util.Arrays;

public class Waves5 extends Waves {

    {
        setInterval(Duration.seconds(5));

        Wave wave1 = new Wave();
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.4), 1, 10);
        wave1.addSubWave(EnemyType.ENEMY_1, Duration.ZERO, Duration.seconds(0.3), 2, 10);

        Wave wave2 = new Wave();
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.seconds(3), Duration.seconds(0.4), 1, 10);
        wave2.addSubWave(EnemyType.ENEMY_1, Duration.ZERO, Duration.seconds(0.3), 2, 5);

        setWaves(Arrays.asList(wave1, wave2));
    }

}
