package application.game.components.managers;

import application.game.components.GameField;
import application.game.components.animation.FollowAnimation;
import application.game.components.managers.enemies.EnemiesManager;
import application.game.components.managers.enemies.Enemy;
import application.game.towers.Tower;
import application.game.towers.Tower1;
import application.game.towers.Tower2;
import application.game.towers.Tower3;
import application.game.utils.Utils;
import application.game.weapons.Bullet;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TowerManager {

    @Autowired
    private GameField gameField;

    @Autowired
    private EnemiesManager enemiesManager;

    public Tower getTower(int towerIndex) {
        switch (towerIndex) {
            case 1: return new Tower1();
            case 2: return new Tower2();
            case 3: return new Tower3();

            default: throw new IllegalArgumentException("Unknown tower index!");
        }
    }

    public Tooltip getTowerDescription(int towerIndex) {
        switch (towerIndex) {
            case 1: return new Tower1().getDescriptionTip();
            case 2: return new Tower2().getDescriptionTip();
            case 3: return new Tower3().getDescriptionTip();

            default: throw new IllegalArgumentException("Unknown tower index!");
        }
    }

    public void start(Tower tower) {
        Duration sleepTime = Duration.seconds(1 / tower.getWeapon().getFireRate());
        Utils.runPeriodicallyInFXThread(sleepTime, () -> shoot(tower), tower::isStopped);
    }

    private void shoot(Tower tower) {
        List<Enemy> enemiesInRange = enemiesManager.getEnemiesInTowerRange(tower);

        if (enemiesInRange.isEmpty()) {
            return;
        }

        Enemy targetEnemy = enemiesInRange.get(0);
        enemiesInRange.remove(targetEnemy);

        Bullet bullet = tower.getWeapon().getBullet();
        ImageView bulletImage = new ImageView(bullet.getImage());
        gameField.getChildren().add(bulletImage);

        double towerHOffset = tower.getImageView().getImage().getWidth() / 2;
        double towerVOffset = tower.getImageView().getImage().getHeight() / 2;

        double bulletHOffset = bulletImage.getImage().getWidth() / 2;
        double bulletVOffset = bulletImage.getImage().getWidth() / 2;

        bulletImage.setTranslateX(tower.getImageView().getTranslateX() + towerHOffset - bulletHOffset);
        bulletImage.setTranslateY(tower.getImageView().getTranslateY() + towerVOffset - bulletVOffset);

        FollowAnimation followAnimation = new FollowAnimation();

        followAnimation.setNode(bulletImage);
        followAnimation.setTarget(targetEnemy);
        followAnimation.setSpeed(bullet.getSpeed());
        followAnimation.disableFollowingAlways();
        followAnimation.setRotateWithDirection(true);
        followAnimation.setOnFinish(() -> {
            gameField.getChildren().remove(bulletImage);
            enemiesManager.shot(tower.getWeapon().getDamage(), (Enemy) followAnimation.getTarget());
        });
        followAnimation.setOnHandle(() -> {
            if (targetEnemy.isDied()) {
                List<Enemy> refreshedEnemiesInRange = enemiesManager.getEnemiesInTowerRange(tower);
                if (!refreshedEnemiesInRange.isEmpty()) {
                    followAnimation.setTarget(refreshedEnemiesInRange.get(0));
                }
            }
        });
        followAnimation.play();

    }

}
