package application.game.components.managers;

import javafx.geometry.Dimension2D;
import javafx.scene.image.Image;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
public class ImagesManager {

    public Image loadImage(String url, double width, double height) {
        return new Image(url, width, height, true, true);
    }

    public Image loadImage(String url, double size) {
        return loadImage(url, size, size);
    }

    public Image loadImage(String url, Dimension2D dimension) {
        return loadImage(url, dimension.getWidth(), dimension.getHeight());
    }

    public Image loadImage(InputStream imageStream, Dimension2D targetImageDimension) {
        return new Image(
                imageStream,
                targetImageDimension.getWidth(),
                targetImageDimension.getHeight(),
                true,
                true
        );
    }

}
