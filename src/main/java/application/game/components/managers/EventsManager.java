package application.game.components.managers;

import application.game.components.GameField;
import application.game.components.animation.Offset;
import application.game.utils.AllowedZone;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EventsManager {

    private double scale = 1;

    @Autowired
    private GameField gameField;

    public void makeDraggable(Node parent, Node toDrag) {
        makeDraggable(parent, toDrag, new AllowedZone(gameField.propertyDragX, null, gameField.propertyDragY, null));
    }

    public void makeDraggable(Node parent, Node toDrag, AllowedZone allowedZone) {
        makeDraggable(parent, toDrag, MouseButton.PRIMARY, allowedZone);
    }

    public void makeDraggable(Node parent, Node toDrag, MouseButton mouseButton, AllowedZone allowedZone) {
        disableDrag(parent);
        CursorPosition cursorPosition = new CursorPosition();

        parent.setOnMouseMoved(event -> {
            cursorPosition.x = event.getX();
            cursorPosition.y = event.getY();
        });

        parent.setOnMouseDragged(event -> {
            if (event.getButton() != mouseButton) return;

            if (cursorPosition.x == 0 || cursorPosition.y == 0) {
                cursorPosition.x = event.getX();
                cursorPosition.y = event.getY();
            }

            double dx = cursorPosition.x - event.getX();
            double dy = cursorPosition.y - event.getY();

            double nextX = toDrag.getTranslateX() - dx;
            double nextY = toDrag.getTranslateY() - dy;

            if (allowedZone.allowedX(nextX)) {
                toDrag.setTranslateX(nextX);
            }

            if (allowedZone.allowedY(nextY)) {
                toDrag.setTranslateY(nextY);
            }

            cursorPosition.x = event.getX();
            cursorPosition.y = event.getY();
        });
    }

    public void makeScrollable(Node parent, Node toDrag) {
        parent.setOnScroll(event -> {
            if (event.getDeltaY() == 0) return;
            scale *= event.getDeltaY() > 0 ? 1.1 : 1 / 1.1;

            toDrag.setScaleX(scale);
            toDrag.setScaleY(scale);
        });
    }

    public void makeMovableByCursor(Pane parent, Node toMove, Offset offset, Runnable action) {
        parent.setOnMouseMoved(event -> {
            toMove.setTranslateX(event.getX() - offset.getHorizontal());
            toMove.setTranslateY(event.getY() - offset.getVertical());

            parent.setOnMouseMoved(e -> {

                double x;
                double y;

                if (offset == null) {
                    x = e.getX();
                    y = e.getY();
                } else {
                    x = e.getX() - offset.getHorizontal();
                    y = e.getY() - offset.getVertical();
                }

                toMove.setTranslateX(x);
                toMove.setTranslateY(y);
                action.run();
            });

        });
    }

    public void disableDrag(Node parent) {
        parent.setOnMouseMoved(null);
        parent.setOnMouseDragged(null);
    }

    public void disableScroll(Node parent) {
        parent.setOnScroll(null);
    }

    public void disableMovement(Node parent) {
        parent.setOnMouseMoved(null);
    }

    public void disableMovement(Node parent, Runnable runnable) {
        disableMovement(parent);
        runnable.run();
    }


    private class CursorPosition {
        private double x;
        private double y;
    }

}
