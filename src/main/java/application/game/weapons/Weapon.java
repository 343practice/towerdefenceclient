package application.game.weapons;

import javafx.scene.image.Image;

public class Weapon {

    private double shootRadius;
    private double damage;
    private double fireRate;
    private Bullet bullet;

    public Weapon(Image bulletImage, double bulletSpeed, double shootRadius, double damage, double fireRate) {
        this(new Bullet(bulletImage, bulletSpeed), shootRadius, damage, fireRate);
    }

    public Weapon(Bullet bullet, double shootRadius, double damage, double fireRate) {
        this.bullet = bullet;
        this.shootRadius = shootRadius;
        this.damage = damage;
        this.fireRate = fireRate;
    }

    public Bullet getBullet() {
        return bullet;
    }

    public double getShootRadius() {
        return shootRadius;
    }

    public double getDamage() {
        return damage;
    }

    public double getFireRate() {
        return fireRate;
    }
}
