package application.game.weapons;

import javafx.scene.image.Image;

public class Bullet {

    private Image image;
    private double speed;

    public Bullet(Image image, double speed) {
        this.image = image;
        this.speed = speed;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
