package application.game.gif_images;

import application.game.components.managers.TransitionManager;
import application.game.utils.Utils;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

public class GifImage extends ImageView {

    private List<Image> frames = new ArrayList<>();
    private boolean repeat = true;
    private int sleepTimeInMillis = 300;
    private boolean stopped;

    public GifImage() {
    }

    public GifImage(List<Image> images, boolean repeat, int sleepTimeInMillis) {
        this.frames = images;
        this.repeat = repeat;
        this.sleepTimeInMillis = sleepTimeInMillis;
    }

    public List<Image> getFrames() {
        return frames;
    }

    public void setFrames(List<Image> frames) {
        this.frames = frames;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public int getSleepTimeInMillis() {
        return sleepTimeInMillis;
    }

    public void setSleepTimeInMillis(int sleepTimeInMillis) {
        this.sleepTimeInMillis = sleepTimeInMillis;
    }

    public void playGif() {
        stopped = false;

        if (frames.size() <= 1) {
            if (!frames.isEmpty()) {
                setImage(frames.get(0));
            }
            return;
        }

        Utils.inNewThread(() -> {
            frames.forEach(image -> {
                if (stopped) {
                    return;
                }
                Platform.runLater(() -> setImage(image));
                Utils.sleep(Duration.millis(sleepTimeInMillis));
            });
            if (repeat && !TransitionManager.isPause()) playGif();
        });
    }

    public void stopGif() {
        this.stopped = true;
    }

}
