package application.game.utils;

@FunctionalInterface
public interface Condition {
    boolean isForbidden();
}
