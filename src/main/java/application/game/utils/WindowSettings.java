package application.game.utils;

import javafx.stage.Screen;

public class WindowSettings {

    private static final Screen SCREEN = Screen.getPrimary();

    public static final double WIDTH = SCREEN.getBounds().getWidth();
    public static final double HEIGHT = SCREEN.getBounds().getHeight();

    public static final double MAX = Math.max(WIDTH, HEIGHT);
    public static final double MIN = Math.min(WIDTH, HEIGHT);

    public static final boolean WIDTH_MAX = WIDTH == MAX;
    public static final boolean HEIGHT_MAX = HEIGHT == MAX;

    public static final boolean WIDTH_MIN = WIDTH == MIN;
    public static final boolean HEIGHT_MIN = HEIGHT == MIN;

    public static final double RATIO = WIDTH / HEIGHT;

}