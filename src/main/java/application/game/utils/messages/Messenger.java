package application.game.utils.messages;

import javafx.application.Platform;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.springframework.stereotype.Component;

@Component
public class Messenger {

    private final Duration messageDuration = Duration.seconds(2);

    public void showMessage(Message.Type messageType, String title, String text) {
        if (title == null || title.isEmpty()) return;
        if (text == null || text.isEmpty()) return;

        Platform.runLater(() -> {
            switch (messageType) {
                case ERROR: showError(title, text); break;
                case INFORMATION: showInformation(title, text); break;
                case WARNING: showWarning(title, text); break;
            }
        });
    }

    public void showMessage(Message message) {
        showMessage(message.getType(), "", message.getText());
    }

    private void showError(String title, String text) {
        Notifications.create()
                .title(title)
                .text(text)
                .hideAfter(messageDuration)
                .showError();
    }

    private void showInformation(String title, String text) {
        Notifications.create()
                .title(title)
                .text(text)
                .hideAfter(messageDuration)
                .showInformation();
    }

    private void showWarning(String title, String text) {
        Notifications.create()
                .title(title)
                .text(text)
                .hideAfter(messageDuration)
                .showWarning();
    }

}
