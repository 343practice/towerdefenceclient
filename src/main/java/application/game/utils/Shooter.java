package application.game.utils;

import application.game.components.managers.enemies.Enemy;
import javafx.geometry.Point2D;

public interface Shooter {
    void shoot(Point2D startPoint, Enemy enemy);
}
