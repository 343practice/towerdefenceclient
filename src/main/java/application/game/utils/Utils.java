package application.game.utils;

import application.game.components.managers.TransitionManager;
import javafx.application.Platform;
import javafx.scene.text.Font;
import javafx.geometry.Point2D;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utils {

    public final Font GAME_FONT_LARGE = Font.loadFont(getClass().getResourceAsStream("/game/font.ttf"), 48);
    public final Font GAME_FONT_MEDIUM = Font.loadFont(getClass().getResourceAsStream("/game/font.ttf"), 36);
    public final Font GAME_FONT_SMALL = Font.loadFont(getClass().getResourceAsStream("/game/font.ttf"), 24);

    public static void sleep(Duration duration) {
        sleep((int) duration.toMillis());
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void inNewThread(Runnable runnable) {
        new Thread(runnable).start();
    }

    @SafeVarargs
    public static <T> List<T> toList(T... elements) {
        if (elements == null) return null;
        return new ArrayList<>(Arrays.asList(elements));
    }

    public static void pause(Condition condition) {
        while (condition.isForbidden()) {
            sleep(0);
        }
    }

    public static void delay(Duration delay, Runnable onFinish, boolean inFXThread) {
        Utils.inNewThread(() -> {
            sleep(delay);
            if (inFXThread) {
                Platform.runLater(onFinish);
            } else onFinish.run();
        });
    }

    public static void pause(Duration duration, Condition condition) {
        final long STEP = 2;
        long millis = (long) duration.toMillis();

        for (int i = 0; i < millis; i += STEP) {
            while (condition.isForbidden()) {
                sleep(0);
            }

            sleep(STEP);
        }
    }

    public static void runPeriodicallyInFXThread(Duration sleepTime, Runnable action, Condition condition) {
        inNewThread(() -> {
            while (!condition.isForbidden()) {
                Platform.runLater(action);
                pause(sleepTime, TransitionManager::isPause);
            }
        });
    }

    public static double getAngle_Radians(Point2D nodePoint, Point2D targetPoint) {
        return getAngle_Radians(targetPoint.getX() - nodePoint.getX(), targetPoint.getY() - nodePoint.getY());
    }

    public static double getAngle_Radians(double deltaX, double deltaY) {
        return Math.atan2(deltaY, deltaX);
    }

}
