package application.game.utils;

import javafx.scene.shape.PathElement;
import application.game.shapes.Path;

public class PathBuilder {

    private Path path = new Path();

    public PathBuilder addElement(PathElement pathElement) {
        path.getElements().add(pathElement);
        return this;
    }

    public Path get() {
        return path;
    }

}