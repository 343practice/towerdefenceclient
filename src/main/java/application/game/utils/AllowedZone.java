package application.game.utils;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class AllowedZone {

    private final SimpleDoubleProperty minX;
    private final ReadOnlyDoubleProperty maxX;
    private final ReadOnlyDoubleProperty minY;
    private final ReadOnlyDoubleProperty maxY;

    public AllowedZone(SimpleDoubleProperty minX, ReadOnlyDoubleProperty maxX, ReadOnlyDoubleProperty minY, ReadOnlyDoubleProperty maxY) {
        this.minX = minX == null ? new SimpleDoubleProperty(0) : minX;
        this.maxX = maxX == null ? new SimpleDoubleProperty(0) : maxX;
        this.minY = minY == null ? new SimpleDoubleProperty(0) : minY;
        this.maxY = maxY == null ? new SimpleDoubleProperty(0) : maxY;
    }

    public boolean allowedX(double x) {
        if (minX.get() == 0 && maxX.get() == 0) return false;
        if (minX.get() > maxX.get()) return false;

        return x > minX.get() && x < maxX.get();
    }

    public boolean allowedY(double y) {
        if (minY.get() == 0 && maxY.get() == 0) return false;
        if (minY.get() > maxY.get()) return false;

        return y > minY.get()&& y < maxY.get();
    }

}
