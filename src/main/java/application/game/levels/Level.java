package application.game.levels;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import application.game.components.managers.enemies.waves.Waves;
import javafx.scene.image.Image;
import application.game.shapes.Path;

public class Level {

    public IntegerProperty indexProperty = new SimpleIntegerProperty();
    private Image image;
    private Path forbiddenZone;
    private Path enemiesPath;
    private Waves waves;
    private double initialMoney;
    private double initialHealth;

    public IntegerProperty waveProperty = new SimpleIntegerProperty();
    public DoubleProperty moneyProperty = new SimpleDoubleProperty();
    public DoubleProperty healthProperty = new SimpleDoubleProperty();

    private String imageResourceName;

    public Level() {
    }

    public int getIndex() {
        return indexProperty.get();
    }

    public void setIndex(int index) {
        indexProperty.set(index);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Path getForbiddenZone() {
        return forbiddenZone;
    }

    public void setForbiddenZone(Path forbiddenZones) {
        this.forbiddenZone = forbiddenZones;
    }

    public Path getEnemiesPath() {
        return enemiesPath;
    }

    public void setEnemiesPath(Path enemiesPath) {
        this.enemiesPath = enemiesPath;
    }

    public Waves getWaves() {
        return waves;
    }

    public void setWaves(Waves waves) {
        this.waves = waves;
    }

    public int getMaxWaves() {
        return waves.getWaves().size();
    }

    public int getCurrentWave() {
        return waveProperty.get();
    }

    public void setCurrentWave(int currentWave) {
        waveProperty.set(currentWave);
    }

    public void setInitialMoney(double initialMoney) {
        this.initialMoney = initialMoney;
        resetMoney();
    }

    public void setInitialHealth(double initialHealth) {
        this.initialHealth = initialHealth;
        resetHealth();
    }

    public double getMoney() {
        return moneyProperty.get();
    }

    public void addMoney(double money) {
        moneyProperty.set(moneyProperty.get() + money);
    }

    public void subtractMoney(double money) {
        moneyProperty.set(moneyProperty.get() - money);
    }

    public double getHealth() {
        return healthProperty.get();
    }

    public void addHealth(double health) {
        healthProperty.set(healthProperty.get() + health);
    }

    public void subtractHealth(double health) {
        if (health > 0) {
            double nextValue = healthProperty.get() - health;

            if (nextValue < 0) {
                nextValue = 0;
            }

            healthProperty.set(nextValue);
        }
    }

    public void setImageResourceName(String imageResourceName) {
        this.imageResourceName = imageResourceName;
    }

    public String getImageResourceName() {
        return imageResourceName;
    }

    public void nextWave() {
        waveProperty.set(waveProperty.get() + 1);
    }

    public void resetMoney() {
        moneyProperty.setValue(initialMoney);
    }

    public void resetHealth() {
        healthProperty.setValue(initialHealth);
    }
}