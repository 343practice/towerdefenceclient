package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way2 extends Way {

    //2560x1080

    @Override
    public Path getForbiddenZone() {
        Path path = new Path();
        path.getElements().add(new MoveTo(0,300));
        path.getElements().add(new LineTo(400,300));
        path.getElements().add(new LineTo(400,500));
        path.getElements().add(new LineTo(700,500));
        path.getElements().add(new LineTo(700,800));
        path.getElements().add(new LineTo(1200,800));
        path.getElements().add(new LineTo(1200,100));
        path.getElements().add(new LineTo(2100,100));
        path.getElements().add(new LineTo(2100,400));
        path.getElements().add(new LineTo(1800,400));
        path.getElements().add(new LineTo(1800,900));
        path.getElements().add(new LineTo(2560,900));

        path.getElements().add(new LineTo(2560,1000));
        path.getElements().add(new LineTo(1700,1000));
        path.getElements().add(new LineTo(1700,300));
        path.getElements().add(new LineTo(2000,300));
        path.getElements().add(new LineTo(2000,200));
        path.getElements().add(new LineTo(1300,200));
        path.getElements().add(new LineTo(1300,900));
        path.getElements().add(new LineTo(600,900));
        path.getElements().add(new LineTo(600,600));
        path.getElements().add(new LineTo(300,600));
        path.getElements().add(new LineTo(300,400));
        path.getElements().add(new LineTo(0,400));

        path.getElements().add(new ClosePath());

        return path;
    }

}
