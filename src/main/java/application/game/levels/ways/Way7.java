package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way7 extends Way {
//1920x1200
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 1000));
        path.getElements().add(new LineTo(500,1000));
        path.getElements().add(new LineTo(500,400));
        path.getElements().add(new LineTo(1300,400));
        path.getElements().add(new LineTo(1300,1200));

        path.getElements().add(new LineTo(1200,1200));
        path.getElements().add(new LineTo(1200,500));
        path.getElements().add(new LineTo(600,500));
        path.getElements().add(new LineTo(600,1100));
        path.getElements().add(new LineTo(0, 1100));
        path.getElements().add(new ClosePath());

        return path;
    }

}
