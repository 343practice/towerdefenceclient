package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way6 extends Way {
//3440x1440
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(500, 0));
        path.getElements().add(new LineTo(500,200));
        path.getElements().add(new LineTo(200,200));
        path.getElements().add(new LineTo(200,1000));
        path.getElements().add(new LineTo(1000,1000));
        path.getElements().add(new LineTo(1000,500));
        path.getElements().add(new LineTo(2100,500));
        path.getElements().add(new LineTo(2100,200));
        path.getElements().add(new LineTo(2900,200));
        path.getElements().add(new LineTo(2900,1200));
        path.getElements().add(new LineTo(2600,1200));
        path.getElements().add(new LineTo(2600,700));
        path.getElements().add(new LineTo(1600,700));
        path.getElements().add(new LineTo(1600,1440));
//
        path.getElements().add(new LineTo(1700,1440));
        path.getElements().add(new LineTo(1700,800));
        path.getElements().add(new LineTo(2500,800));
        path.getElements().add(new LineTo(2500,1300));
        path.getElements().add(new LineTo(3000,1300));
        path.getElements().add(new LineTo(3000,100));
        path.getElements().add(new LineTo(2000,100));
        path.getElements().add(new LineTo(2000,400));
        path.getElements().add(new LineTo(900,400));
        path.getElements().add(new LineTo(900,900));
        path.getElements().add(new LineTo(300,900));
        path.getElements().add(new LineTo(300,300));
        path.getElements().add(new LineTo(600,300));
        path.getElements().add(new LineTo(600, 0));
        path.getElements().add(new ClosePath());

        return path;
    }

}
