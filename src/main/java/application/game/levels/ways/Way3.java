package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way3 extends Way {
//1920x1200
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 100));
        path.getElements().add(new LineTo(300,100));
        path.getElements().add(new LineTo(300,1000));
        path.getElements().add(new LineTo(1600,1000));
        path.getElements().add(new LineTo(1600,200));
        path.getElements().add(new LineTo(1000,200));
        path.getElements().add(new LineTo(1000,700));
        path.getElements().add(new LineTo(400,700));
        path.getElements().add(new LineTo(400,0));

        path.getElements().add(new LineTo(500,0));
        path.getElements().add(new LineTo(500,600));
        path.getElements().add(new LineTo(900,600));
        path.getElements().add(new LineTo(900,100));
        path.getElements().add(new LineTo(1700,100));
        path.getElements().add(new LineTo(1700,1100));
        path.getElements().add(new LineTo(200,1100));
        path.getElements().add(new LineTo(200,200));
        path.getElements().add(new LineTo(0, 200));
        path.getElements().add(new ClosePath());

        return path;
    }

}
