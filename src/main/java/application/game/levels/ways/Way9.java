package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way9 extends Way {
//3000x1687
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 100));
        path.getElements().add(new LineTo(300,100));
        path.getElements().add(new LineTo(300,1300));
        path.getElements().add(new LineTo(2500,1300));
        path.getElements().add(new LineTo(2500,300));
        path.getElements().add(new LineTo(1600,300));
        path.getElements().add(new LineTo(1600,1100));
        path.getElements().add(new LineTo(500,1100));
        path.getElements().add(new LineTo(500,0));

        path.getElements().add(new LineTo(600,0));
        path.getElements().add(new LineTo(600,1000));
        path.getElements().add(new LineTo(1500,1000));
        path.getElements().add(new LineTo(1500,200));
        path.getElements().add(new LineTo(2600,200));
        path.getElements().add(new LineTo(2600,1400));
        path.getElements().add(new LineTo(200,1400));
        path.getElements().add(new LineTo(200,200));
        path.getElements().add(new LineTo(0, 200));
        path.getElements().add(new ClosePath());

        return path;
    }

}
