package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way10 extends Way {
//1920x1080
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, -100));
        path.getElements().add(new LineTo(300,200));
        path.getElements().add(new LineTo(1500,200));
        path.getElements().add(new LineTo(1500,700));
        path.getElements().add(new LineTo(700,700));
        path.getElements().add(new LineTo(700,500));
        path.getElements().add(new LineTo(300,500));
        path.getElements().add(new LineTo(300,1080));

        path.getElements().add(new LineTo(200,1080));
        path.getElements().add(new LineTo(200,400));
        path.getElements().add(new LineTo(800,400));
        path.getElements().add(new LineTo(800,600));
        path.getElements().add(new LineTo(1400,600));
        path.getElements().add(new LineTo(1400,300));
        path.getElements().add(new LineTo(200,300));
        path.getElements().add(new LineTo(-100, 0));
        path.getElements().add(new ClosePath());

        return path;
    }

}
