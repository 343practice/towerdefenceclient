package application.game.levels.ways;

import javafx.geometry.Point2D;
import application.game.shapes.Path;

import java.util.ArrayList;
import java.util.List;

public abstract class Way {

    public abstract Path getForbiddenZone();
    public Path getEnemyPath() {
        List<Point2D> points = Path.getPoints(getForbiddenZone().getElements());
        List<Point2D> enemyPath = new ArrayList<>(points.size() / 2);

        for (int i = 0; i < points.size() / 2; i++) {
            int reverse = points.size() - i - 1;
            Point2D point1 = points.get(i);
            Point2D point2 = points.get(reverse);

            enemyPath.add(point1.midpoint(point2));
        }

        return Path.getPath(enemyPath);
    }

}
