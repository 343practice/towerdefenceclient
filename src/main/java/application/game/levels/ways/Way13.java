package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way13 extends Way {
//1920x1200
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 200));
        path.getElements().add(new LineTo(1000,200));
        path.getElements().add(new LineTo(1000,900));
        path.getElements().add(new LineTo(600,900));
        path.getElements().add(new LineTo(600,1050));
        path.getElements().add(new LineTo(1500,1050));
        path.getElements().add(new LineTo(1500,0));

        path.getElements().add(new LineTo(1600,0));
        path.getElements().add(new LineTo(1600,1150));
        path.getElements().add(new LineTo(500,1150));
        path.getElements().add(new LineTo(500,800));
        path.getElements().add(new LineTo(900,800));
        path.getElements().add(new LineTo(900,300));
        path.getElements().add(new LineTo(0, 300));
        path.getElements().add(new ClosePath());

        return path;
    }

}
