package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way11 extends Way {
//4992x3648
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(-100, 3648));
        path.getElements().add(new LineTo(200,3348));
        path.getElements().add(new LineTo(3000,3348));
        path.getElements().add(new LineTo(3000,3348));
        path.getElements().add(new LineTo(1500,1500));
        path.getElements().add(new LineTo(1500,500));
        path.getElements().add(new LineTo(4000,500));
        path.getElements().add(new LineTo(4000,3648));

        path.getElements().add(new LineTo(3900,3648));
        path.getElements().add(new LineTo(3900,600));
        path.getElements().add(new LineTo(1600,600));
        path.getElements().add(new LineTo(1600,1500));
        path.getElements().add(new LineTo(3100,3348));
        path.getElements().add(new LineTo(3100,3448));
        path.getElements().add(new LineTo(300,3448));
        path.getElements().add(new LineTo(0, 3748));
        path.getElements().add(new ClosePath());

        return path;
    }

}
