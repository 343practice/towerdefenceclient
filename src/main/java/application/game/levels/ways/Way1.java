package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way1 extends Way {

    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 1000));
        path.getElements().add(new LineTo(600, 1000));
        path.getElements().add(new LineTo(600, 700));
        path.getElements().add(new LineTo(100, 700));
        path.getElements().add(new LineTo(100, 100));
        path.getElements().add(new LineTo(300, 100));
        path.getElements().add(new LineTo(300, 600));
        path.getElements().add(new LineTo(1400, 600));
        path.getElements().add(new LineTo(1400, 100));
        path.getElements().add(new LineTo(1600, 100));
        path.getElements().add(new LineTo(1600, 1000));
        path.getElements().add(new LineTo(1920, 1000));

        path.getElements().add(new LineTo(1920, 900));
        path.getElements().add(new LineTo(1700, 900));
        path.getElements().add(new LineTo(1700, 0));
        path.getElements().add(new LineTo(1300, 0));
        path.getElements().add(new LineTo(1300, 500));
        path.getElements().add(new LineTo(400, 500));
        path.getElements().add(new LineTo(400, 0));
        path.getElements().add(new LineTo(0, 0));
        path.getElements().add(new LineTo(0, 800));
        path.getElements().add(new LineTo(500, 800));
        path.getElements().add(new LineTo(500, 900));
        path.getElements().add(new LineTo(0, 900));
        path.getElements().add(new ClosePath());

        return path;
    }

}
