package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way14 extends Way {
//1920x1080
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 1000));
        path.getElements().add(new LineTo(600,1000));
        path.getElements().add(new LineTo(600,300));
        path.getElements().add(new LineTo(300,300));
        path.getElements().add(new LineTo(300,200));
        path.getElements().add(new LineTo(1400,200));
        path.getElements().add(new LineTo(1400,1080));

        path.getElements().add(new LineTo(1500,1080));
        path.getElements().add(new LineTo(1500,100));
        path.getElements().add(new LineTo(200,100));
        path.getElements().add(new LineTo(200,400));
        path.getElements().add(new LineTo(500,400));
        path.getElements().add(new LineTo(500,900));
        path.getElements().add(new LineTo(0, 900));
        path.getElements().add(new ClosePath());

        return path;
    }

}
