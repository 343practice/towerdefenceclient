package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way15 extends Way {
//2560x1600
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 100));
        path.getElements().add(new LineTo(2400,100));
        path.getElements().add(new LineTo(2400,1300));
        path.getElements().add(new LineTo(1000,1300));
        path.getElements().add(new LineTo(1000,900));
        path.getElements().add(new LineTo(1800,900));
        path.getElements().add(new LineTo(1800,500));
        path.getElements().add(new LineTo(500,500));
        path.getElements().add(new LineTo(500,1600));

        path.getElements().add(new LineTo(400,1600));
        path.getElements().add(new LineTo(400,400));
        path.getElements().add(new LineTo(1900,400));
        path.getElements().add(new LineTo(1900,1000));
        path.getElements().add(new LineTo(1100,1000));
        path.getElements().add(new LineTo(1100,1200));
        path.getElements().add(new LineTo(2300,1200));
        path.getElements().add(new LineTo(2300,200));
        path.getElements().add(new LineTo(0, 200));
        path.getElements().add(new ClosePath());

        return path;
    }

}
