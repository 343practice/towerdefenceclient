package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way4 extends Way {
//1920x1080
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(100, 1080));
        path.getElements().add(new LineTo(100,600));
        path.getElements().add(new LineTo(500,600));
        path.getElements().add(new LineTo(500,200));
        path.getElements().add(new LineTo(1300,200));
        path.getElements().add(new LineTo(1300,500));
        path.getElements().add(new LineTo(1700,500));
        path.getElements().add(new LineTo(1700,1080));

        path.getElements().add(new LineTo(1600,1080));
        path.getElements().add(new LineTo(1600,600));
        path.getElements().add(new LineTo(1200,600));
        path.getElements().add(new LineTo(1200,300));
        path.getElements().add(new LineTo(600,300));
        path.getElements().add(new LineTo(600,700));
        path.getElements().add(new LineTo(200,700));
        path.getElements().add(new LineTo(200, 1080));
        path.getElements().add(new ClosePath());

        return path;
    }

}
