package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way12 extends Way {
//1920x1200
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 900));
        path.getElements().add(new LineTo(1500,900));
        path.getElements().add(new LineTo(1500,700));
        path.getElements().add(new LineTo(300,700));
        path.getElements().add(new LineTo(300,300));
        path.getElements().add(new LineTo(1920,300));

        path.getElements().add(new LineTo(1920,400));
        path.getElements().add(new LineTo(400,400));
        path.getElements().add(new LineTo(400,600));
        path.getElements().add(new LineTo(1600,600));
        path.getElements().add(new LineTo(1600,1000));
        path.getElements().add(new LineTo(0, 1000));
        path.getElements().add(new ClosePath());

        return path;
    }

}
