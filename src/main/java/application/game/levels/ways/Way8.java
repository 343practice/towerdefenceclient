package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way8 extends Way {
//2560x1440
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(0, 700));
        path.getElements().add(new LineTo(200,700));
        path.getElements().add(new LineTo(200,300));
        path.getElements().add(new LineTo(2000,300));
        path.getElements().add(new LineTo(2000,1100));
        path.getElements().add(new LineTo(1400,1100));
        path.getElements().add(new LineTo(1400,600));
        path.getElements().add(new LineTo(800,600));
        path.getElements().add(new LineTo(800,1440));

        path.getElements().add(new LineTo(700,1440 ));
        path.getElements().add(new LineTo(700,500));
        path.getElements().add(new LineTo(1500,500));
        path.getElements().add(new LineTo(1500,1000));
        path.getElements().add(new LineTo(1900,1000));
        path.getElements().add(new LineTo(1900,400));
        path.getElements().add(new LineTo(300,400));
        path.getElements().add(new LineTo(300,800));
        path.getElements().add(new LineTo(0, 800));
        path.getElements().add(new ClosePath());

        return path;
    }

}
