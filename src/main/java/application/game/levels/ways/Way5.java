package application.game.levels.ways;

import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import application.game.shapes.Path;

public class Way5 extends Way {
//2000x1333
    @Override
    public Path getForbiddenZone() {
        Path path = new Path();

        path.getElements().add(new MoveTo(100, 0));
        path.getElements().add(new LineTo(100,400));
        path.getElements().add(new LineTo(300,400));
        path.getElements().add(new LineTo(300,800));
        path.getElements().add(new LineTo(800,800));
        path.getElements().add(new LineTo(800,200));
        path.getElements().add(new LineTo(1300,200));
        path.getElements().add(new LineTo(1300,1333));

        path.getElements().add(new LineTo(1400,1333));
        path.getElements().add(new LineTo(1400,100));
        path.getElements().add(new LineTo(700,100));
        path.getElements().add(new LineTo(700,700));
        path.getElements().add(new LineTo(400,700));
        path.getElements().add(new LineTo(400,300));
        path.getElements().add(new LineTo(200,300));
        path.getElements().add(new LineTo(200, 0));
        path.getElements().add(new ClosePath());

        return path;
    }

}
